using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public Camera cam;
    private float xRotation = 0f;

    public float xSensitivity = 30f;
    public float ySensitivity = 30f;

    private GameManager gameManager;

    [SerializeField] private Transform arms;
    private void Start()
    {
        LoadSens();
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }
    public void ProcessLook(Vector2 input)
    {
        if (gameManager.inUI) return;
        float mouseX = input.x;
        float mouseY = input.y;
        xRotation -= (mouseY * Time.deltaTime) * ySensitivity;
        xRotation = Mathf.Clamp(xRotation, -80f, 80f);

        // Kamera Rotation
        cam.transform.localRotation = Quaternion.Euler(xRotation, 0, 0);

        transform.Rotate(Vector3.up * (mouseX * Time.deltaTime) * xSensitivity);

        // Arm Rotation
        arms.localRotation = Quaternion.Euler(new Vector3(xRotation, 0, 0));
    }
    public void LoadSens()
    {
        xSensitivity = PlayerPrefs.GetFloat("mouseSensitiviy");
        ySensitivity = PlayerPrefs.GetFloat("mouseSensitiviy");
    }
}
