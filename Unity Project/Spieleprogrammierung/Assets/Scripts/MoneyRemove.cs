using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyRemove : MonoBehaviour
{
    private int money;
    private TextMeshProUGUI moneyUIAnimRemove;
    private float elapsedTime;
    private Vector3 starPos;
    private Vector3 endPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void SetMoney(int money)
    {
        
        moneyUIAnimRemove = GetComponent<TextMeshProUGUI>();
        moneyUIAnimRemove.rectTransform.position = new Vector3(moneyUIAnimRemove.rectTransform.position.x, moneyUIAnimRemove.rectTransform.position.y - 20, moneyUIAnimRemove.rectTransform.position.z);
        starPos = moneyUIAnimRemove.rectTransform.position;
        endPos = new Vector3(starPos.x, starPos.y - 20, starPos.z);
        this.money = money;
        moneyUIAnimRemove.text = $"-{money}$";
        Destroy(this.gameObject, 2f);
    }

    // Update is called once per frame
    void Update()
    {        
        elapsedTime += Time.deltaTime;
        float percentageComplete = elapsedTime / 1f;
        if (moneyUIAnimRemove.transform.position != endPos)
        {
            moneyUIAnimRemove.transform.position = Vector3.Lerp(starPos, endPos, percentageComplete);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
