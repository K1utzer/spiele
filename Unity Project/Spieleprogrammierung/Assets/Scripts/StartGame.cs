using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    private AsyncOperation sceneToLoad;
    private bool SceneDone = false;
    private AnimationClip[] clips;
	private bool alreadyWatched = false;
	[SerializeField]
	private GameObject skipButton;
	[SerializeField]
	private GameObject loadingText;
	
	private bool IntToBool(int n)
	{
		if(n>=1) return true;
		return false;
	}
	private int BoolToInt(bool b)
	{
		if(b) return 1;
		return 0;
	}
    private void Start()
    {
		loadingText.SetActive(false);
        clips = GetComponent<Animator>().runtimeAnimatorController.animationClips;
        Time.timeScale = 1;		
		skipButton.SetActive(false);
		if(PlayerPrefs.HasKey("alreadyWatched"))
		{
			alreadyWatched = IntToBool(PlayerPrefs.GetInt("alreadyWatched"));
			if(alreadyWatched)
			{
				skipButton.SetActive(true);
			}
		}
        StartCoroutine(WaitForEndAnim());
		StartCoroutine(Load("G2 Map"));
        
    }
	public void SkipButton()
	{
		if(alreadyWatched)
		{
			loadingText.SetActive(true);
			sceneToLoad.allowSceneActivation = true;
			skipButton.SetActive(false);
		}
	}
    IEnumerator Load(string SceneName)
    {
        sceneToLoad = SceneManager.LoadSceneAsync(SceneName);
        sceneToLoad.allowSceneActivation = false;
        while (!sceneToLoad.isDone)
        {
			if(sceneToLoad.allowSceneActivation)
			{
				loadingText.SetActive(true);
				skipButton.SetActive(false);
			}
            yield return new WaitForSeconds(1);
        }  
    }
    IEnumerator WaitForEndAnim()
    {         
        yield return new WaitForSeconds(clips[0].length);
		PlayerPrefs.SetInt("alreadyWatched", BoolToInt(true));
        sceneToLoad.allowSceneActivation = true;
    }

}
