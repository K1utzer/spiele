using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuButtons : MonoBehaviour
{
    public string GameSceneName;

    [SerializeField]
    private Slider volume;
    [SerializeField]
    private TextMeshProUGUI volume_text;
    [SerializeField]
    private Slider sensitivity;
    [SerializeField]
    private TextMeshProUGUI sensitivity_text;

    [SerializeField]
    private GameObject MainMenu;
    [SerializeField]
    private GameObject SettingsMenu;
    [SerializeField]
    private GameObject ControlsMenu;
    [SerializeField]
    private GameObject CredtisMenu;
	[SerializeField]
    private GameObject DifficultyMenu;
    [SerializeField]
    private TextMeshProUGUI HighScoreText;
    private void Start()
    {
        HighScoreText.text = "HighScore\n";
		string[] tmpHighs = new string[]{"Easy","Medium","Hard","Extrem"};
		for(int i = 1; i < tmpHighs.Length+1; i++)
		{
			if(PlayerPrefs.HasKey($"HighScore{i}"))
			{				
				HighScoreText.text += $"{tmpHighs[i-1]}: {PlayerPrefs.GetInt($"HighScore{i}")}\n";
			}else
			{
				HighScoreText.text += $"{tmpHighs[i-1]}: 0\n";
			}
		}
        volume.onValueChanged.AddListener((v) =>
        {
            AudioListener.volume = v;
            volume_text.text = v.ToString("0.0");
            PlayerPrefs.SetFloat("Volume", v);
        });
        sensitivity.onValueChanged.AddListener((v) =>
        {
            sensitivity_text.text = v.ToString("0");
            PlayerPrefs.SetFloat("mouseSensitiviy", v);
        });

        if (PlayerPrefs.HasKey("Volume"))
        {
            volume.value = PlayerPrefs.GetFloat("Volume");
        }
        else
        {
            volume.value = 1.0f;
        }

        if (PlayerPrefs.HasKey("mouseSensitiviy"))
        {
            sensitivity.value = PlayerPrefs.GetFloat("mouseSensitiviy");
        }
        else
        {
            sensitivity.value = 50f;
        }
    }
    public void StartButton()
    {
        //SceneManager.LoadScene(GameSceneName);
        MainMenu.SetActive(false);
		DifficultyMenu.SetActive(true);        
    }
	public void EasyButton()
	{
		Difficulty.wave = 0;
		Difficulty.lives = 1;
		Difficulty.health = 150;
		Difficulty.difficulty = 1;
		LoadMapScene();
	}
	public void MediumButton()
	{
		Difficulty.wave = 0;
		Difficulty.lives = 1;
		Difficulty.health = 100;
		Difficulty.difficulty = 2;
		LoadMapScene();
	}
	public void HardButton()
	{
		Difficulty.wave = 0;
		Difficulty.lives = 1;
		Difficulty.health = 50;
		Difficulty.difficulty = 3;
		LoadMapScene();
	}
	public void ExtremButton()
	{
		Difficulty.wave = 0;
		Difficulty.lives = 1;
		Difficulty.health = 20;
		Difficulty.difficulty = 4;
		LoadMapScene();
	}
	private void LoadMapScene()
	{
		DifficultyMenu.SetActive(false);  
		GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>().Load(GameSceneName);
	}
    public void ControlsButton()
    {
        MainMenu.SetActive(false);
        ControlsMenu.SetActive(true);
    }
    public void SettingsButton()
    {
        MainMenu.SetActive(false);
        SettingsMenu.SetActive(true);
    }
    public void CreditsButton()
    {
        MainMenu.SetActive(false);
        CredtisMenu.SetActive(true);
    }
    public void QuitButton()
    {
        Application.Quit();
    }

    public void SettingsBackButton()
    {
        MainMenu.SetActive(true);
        SettingsMenu.SetActive(false);
        CredtisMenu.SetActive(false);
        ControlsMenu.SetActive(false);
    }
    public void CredtisBackButton()
    {
        MainMenu.SetActive(true);
        CredtisMenu.SetActive(false);
    }
    public void DifficultyBackButton()
    {
        MainMenu.SetActive(true);
        DifficultyMenu.SetActive(false);
    }
}
