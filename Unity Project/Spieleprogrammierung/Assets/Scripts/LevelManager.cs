using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    [SerializeField]
    private GameObject loaderCanvas;
    [SerializeField]
    private Image ProgressBar;
    private AsyncOperation sceneToLoad;
    private float target;
    private void Start()
    {
        Time.timeScale = 1;
    }
    public void Load(string SceneName)
    {
        loaderCanvas.SetActive(true);
        sceneToLoad = SceneManager.LoadSceneAsync(SceneName);
        StartCoroutine(LoadingScreen());
    }
    IEnumerator LoadingScreen()
    {
        while (!sceneToLoad.isDone)
        {
            target = sceneToLoad.progress;
            yield return null;
        }
    }
    private void Update()
    {
        ProgressBar.fillAmount = Mathf.MoveTowards(ProgressBar.fillAmount, target, 2 * Time.deltaTime);
    }

}