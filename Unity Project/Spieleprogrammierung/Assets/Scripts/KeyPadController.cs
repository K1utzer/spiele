using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;

public class KeyPadController : MonoBehaviour
{
    [Header("Floor")]
    public int floor;
    [Header("KeyPadDisplay")]
    public TextMeshProUGUI NumPadDisplay;
    [Header("Code Tipp Spawn Points")]
    public TextMeshProUGUI[] NotForCode;
    [Header("Color Tipp Spawn Points")]
    public GameObject[] ColorCodes;
    [Header("Materials")]
    public Material[] materials;
    [Header("KeyPad")]
    public GameObject TargetChar;
    [Header("Sounds")]
    public AudioSource button_pressed;
    public AudioSource correct_code;
    public AudioSource wrong_code;
    [SerializeField]
    private GameObject door;
    private bool isOpen;
    private int MaxLength = 8;
    private int currentIndex;
    public string Code;
    private bool isActive = true;
    private bool debug = false;

    // Start is called before the first frame update
    void Start()
    {
        isOpen = false;
        currentIndex = MaxLength;
        var rand = new System.Random();
        for (int c = 0; c < MaxLength; c++)
        {
            Code += Convert.ToString(rand.Next(0, 6));
        }
        NumPadDisplay.text = "********";
        
    }
    public string GetCode()
    {
        return Code;
    }
    public void OpenDoor()
    {
        currentIndex = MaxLength;
        NumPadDisplay.text = "********";
        foreach (char c in Code.ToCharArray())
        {
            UpdateDisplay(""+Char.GetNumericValue(c));
        }
        
    }
    public void Pressed(GameObject pressedButton)
    {
        if (!TargetChar.GetComponent<Animation>().isPlaying)
        {
            UpdateDisplay(Regex.Match(pressedButton.name, @"\d+").Value);
            pressedButton.GetComponent<KeyPadAnimations>().Play();
            // Play sound
            button_pressed.Play();
        }       
    }
    private void UpdateDisplay(string numberPad)
    {
        if (debug)
        {
            correct_code.Play();
            Open();
        }
        if (NumPadDisplay.text == "TRY AGAIN")
        {
            NumPadDisplay.text = "********";
        }
        if (isActive)
        {
            if (currentIndex == 1)
            {
                NumPadDisplay.text = InsertText(numberPad);
                if (NumPadDisplay.text == Code)
                {
                    Open();
                    correct_code.Play();
                    return;
                }
                NumPadDisplay.text = "TRY AGAIN";
                currentIndex = MaxLength;
                wrong_code.Play();
                return;
            }
            NumPadDisplay.text = InsertText(numberPad);
            
        }
    }
    private void Open()
    {
        NumPadDisplay.text = "CORRECT";
        isOpen = true;
        //door.GetComponent<Animator>().SetBool("isOpen", isOpen);        
        isActive = false;
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().SetUnlockedLevel(floor+1);
		this.DestroyGameObject(door);
        return;
    }
    private string InsertText(string numberPad)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder(NumPadDisplay.text);
        for(int i = 0; i < MaxLength-1; i++)
        {
            sb[i] = sb[i + 1];
        }
        sb[MaxLength - 1] = Convert.ToChar(numberPad.ToString());
        currentIndex--;
        return sb.ToString();
    }
    public void SetNumberTippInMap()
    {
        var rand = new System.Random();        
        int randomTextField = rand.Next(0, NotForCode.Length);
        if (NotForCode[randomTextField].text.Length > 0)
        {
            NotForCode[randomTextField].text += Code;
        }
        else
        {
            NotForCode[randomTextField].text = Code;
        }
    }
    private void DestroyGameObject(GameObject gameObject)
    {
        Destroy(gameObject);
    }
    public void SetColorTippInMap()
    {
        var rand = new System.Random();
        int randomColorSpawn = rand.Next(0, ColorCodes.Length);

        Renderer[] colorTippObjects = new Renderer[8];
            colorTippObjects = ColorCodes[randomColorSpawn].GetComponentsInChildren<Renderer>();
        char[] arr = Code.ToCharArray();
        for (int i = 0; i < arr.Length; i++)
        {
            colorTippObjects[i].material = materials[arr[i] - '0'];
        }

    }
}
