using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyAdd : MonoBehaviour
{
    private int money;
    private TextMeshProUGUI moneyUIAnimAdd;
    private float elapsedTime;
    private Vector3 starPos;
    private Vector3 endPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void SetMoney(int money)
    {
        moneyUIAnimAdd = GetComponent<TextMeshProUGUI>();
        moneyUIAnimAdd.rectTransform.position = new Vector3(moneyUIAnimAdd.rectTransform.position.x, moneyUIAnimAdd.rectTransform.position.y - 35, moneyUIAnimAdd.rectTransform.position.z);
        starPos = moneyUIAnimAdd.rectTransform.position;
        endPos = new Vector3(starPos.x, starPos.y + 15, starPos.z);
        this.money = money;
        moneyUIAnimAdd.text = $"+{money}$";
        Destroy(this.gameObject, 2f);
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        float percentageComplete = elapsedTime / 1f;
        if(moneyUIAnimAdd.transform.position != endPos)
        {
            moneyUIAnimAdd.transform.position = Vector3.Lerp(starPos, endPos, percentageComplete);
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }
}
