using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomFire : MonoBehaviour
{
    private GameObject MuzzleFlash;
    private AnimationClip[] clips;
    // Start is called before the first frame update
    void Start()
    {
        MuzzleFlash = FindComponentInChildWithTag<GameObject>("MuzzleFlash");        
        clips = GetComponent<Animator>().runtimeAnimatorController.animationClips;
        StartCoroutine(Fireing());
    }

    IEnumerator Fireing()
    {
        yield return new WaitForSeconds(35);
        while (true)
        {
            MuzzleFlash.SetActive(true);
            GetComponent<Animator>().SetTrigger("shoot");
            yield return new WaitForSeconds(clips[0].length+.5f);
            MuzzleFlash.SetActive(false);
            yield return new WaitForSeconds(Random.Range(0,4));
        }
    }
    public GameObject FindComponentInChildWithTag<T>(string tag)
    {

        Transform[] t = GetComponentsInChildren<Transform>(true);
        foreach(Transform tr in t)
        {
            if(tr.tag == tag)
            {
                return tr.gameObject;
            }
        }
        return null;
    }
}