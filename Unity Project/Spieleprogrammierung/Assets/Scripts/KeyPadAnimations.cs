using UnityEngine;
using TMPro;
using System.Text.RegularExpressions;

public class KeyPadAnimations : MonoBehaviour
{
    public AnimationClip pressed;
    public GameObject TargetChar;

    void Start()
    {
        //Bad Fix damit alle buttons Animationen durchlaufen und alle raus stehen, da am anfang einige schon halb gepresst sind
        TargetChar.GetComponent<Animation>().PlayQueued(pressed.name);
    }
    public void Play()
    {      
        TargetChar.GetComponent<Animation>().Play(pressed.name);
    }
}
