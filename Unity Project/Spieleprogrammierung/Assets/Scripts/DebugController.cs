using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugController : MonoBehaviour
{
    [SerializeField]
    private GameObject explosion;

    bool showConsole;
    string input;
    public static DebugCommand KILL_ALL;
    public static DebugCommand<int> ADD_MONEY;
    public static DebugCommand HELP;
    public static DebugCommand<int> SET_WAVE;
    public static DebugCommand<int> GODMODE;
    public static DebugCommand<int> STAMINA;
    public static DebugCommand<int> ZOMBIE_SPAWN_1;
    public static DebugCommand<int> ZOMBIE_SPAWN_2;
    public static DebugCommand<int> ZOMBIE_SPAWN_3;
    public static DebugCommand<int> ZOMBIE_SPAWN_4;
    public static DebugCommand ZOMBIE_LIST;
    public static DebugCommand<int> SPAWN_NAOD;
    public static DebugCommand DOOR_INFO;
    public static DebugCommand<int> DOOR_OPEN;
    public static DebugCommand MEDIC_LIST;
    public static DebugCommand<int> MEDIC_SPAWN;
    public static DebugCommand MEDIC_KILL_ALL;
    public static DebugCommand AMMO_LIST;
    public static DebugCommand<int> AMMO_SPAWN;
    public static DebugCommand AMMO_KILL_ALL;
    public static DebugCommand<int> CHEAT;
    public static DebugCommand<int> FPS_SHOW;



    public List<object> commandList;
    private bool showHelp = false;
    Vector2 scroll;
    private GameManager gameManager;
    private PlayerMotor playerMotor;
    private PlayerHealth playerHealth;
    private bool showCodes = false;
    private bool showZombies = false;
    private bool showMedic = false;
    private bool showAmmo = false;
    private List<string> tippCommands;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        playerMotor = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMotor>();
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();

        KILL_ALL = new DebugCommand("zombie_kill_all", "Removes all Zombies", "zombie_kill_all", () => 
        {
            gameManager.KillAllZombies(explosion);
        });
        ADD_MONEY = new DebugCommand<int>("add_money", "Adds amount of money", "add_money <money_amount>", (x) =>
        {
            playerMotor.AddMoney(x);
        });
        HELP = new DebugCommand("help", "List of commands", "help", () =>
        {
            showHelp = true;
            showCodes = false;
            showZombies = false;
            showMedic = false;
            showAmmo = false;
        });
        SET_WAVE = new DebugCommand<int>("set_wave", "Sets the wave number", "set_wave <wave_number>", (x) =>
        {
            gameManager.round = x;
            gameManager.newWaveSet = true;
            gameManager.KillAllZombies(explosion);
            //gameManager.SetWave(x);
        });
        GODMODE = new DebugCommand<int>("godmode", "Sets godmode on (1) or off (0)", "godmode <number>", (x) =>
        {
            playerHealth.SetGodmode(x);
        });
        STAMINA = new DebugCommand<int>("stamina", "Sets stamina to  unlimited (1) or normal (0)", "stamina <number>", (x) =>
        {
            playerMotor.StaminaUnlimited(x);
        });
        ZOMBIE_SPAWN_1 = new DebugCommand<int>("zombie_spawn_1", "Spawn amount of Zombies of type 1", "zombie_spawn_1 <amount_zombies>", (x) =>
        {
            gameManager.SpawnZombie(0, x);
        });
        ZOMBIE_SPAWN_2 = new DebugCommand<int>("zombie_spawn_2", "Spawn amount of Zombies of type 2", "zombie_spawn_2 <amount_zombies>", (x) =>
        {
            gameManager.SpawnZombie(1, x);
        });
        ZOMBIE_SPAWN_3 = new DebugCommand<int>("zombie_spawn_3", "Spawn amount of Zombies of type 3", "zombie_spawn_3 <amount_zombies>", (x) =>
        {
            gameManager.SpawnZombie(2, x);
        });
        ZOMBIE_SPAWN_4 = new DebugCommand<int>("zombie_spawn_4", "Spawn amount of Zombies of type 4", "zombie_spawn_4 <amount_zombies>", (x) =>
        {
            gameManager.SpawnZombie(3, x);
        });
        SPAWN_NAOD = new DebugCommand<int>("spawn_naod", "Spawn amount of Naod", "spawn_naod <amount_naods>", (x) =>
        {
            gameManager.SpawnNaod(x);
        });
        DOOR_OPEN = new DebugCommand<int>("door_open", "Open door with the index", "door_open <door index>", (x) =>
        {
            OpenDoor(x);
        });
        DOOR_INFO = new DebugCommand("door_info", "Get Info for all the doors", "door_info", () =>
        {           
            showCodes = true;
            showHelp = false;
            showZombies = false;
            showMedic = false;
            showAmmo = false;
        });
        ZOMBIE_LIST = new DebugCommand("zombie_list", "List of all living zombies", "zombie_list", () =>
        {
            showCodes = false;
            showHelp = false;
            showZombies = true;
            showMedic = false;
            showAmmo = false;
        });
        MEDIC_SPAWN = new DebugCommand<int>("medic_spawn", "spawn medic kit with specified amount", "medic_spawn <amount>", (x) =>
        {
            gameManager.SpawnMedicKit(x);
        });
        MEDIC_KILL_ALL = new DebugCommand("medic_kill_all", "Destroy all medic kit", "medic_kill_all", () =>
        {
            gameManager.KillAllMedicKits();
        });
        MEDIC_LIST = new DebugCommand("medic_list", "Lists all active medic kits", "medic_list", () =>
        {
            showCodes = false;
            showHelp = false;
            showZombies = false;
            showMedic = true;
            showAmmo = false;
        });
        AMMO_SPAWN = new DebugCommand<int>("ammo_spawn", "spawn ammo with specified amount", "ammo_spawn <amount>", (x) =>
        {
            gameManager.SpawnAmmunition(x);
        });
        AMMO_KILL_ALL = new DebugCommand("ammo_kill_all", "Destroy all medic kit", "ammo_kill_all", () =>
        {
            gameManager.KillAllAmmo();
        });
        AMMO_LIST = new DebugCommand("ammo_list", "Lists all active medic kits", "ammo_list", () =>
        {
            showCodes = false;
            showHelp = false;
            showZombies = false;
            showMedic = true;
            showAmmo = true;
        });
        CHEAT = new DebugCommand<int>("cheat", "Sets cheat on (1) or off (0)", "cheat <number>", (x) =>
        {
            playerHealth.SetGodmode(x);
            playerMotor.StaminaUnlimited(x);
            if(x == 0)
            {
                playerMotor.RemoveMoney(500000);
            }
            else
            {
                playerMotor.AddMoney(500000);
            }            
            OpenDoor(0);
            OpenDoor(1);
            OpenDoor(2);
        });
        FPS_SHOW = new DebugCommand<int>("fps_show", "Sets fps label on (1) or off (0)", "fps_show <number>", (x) =>
        {
            gameObject.GetComponent<FPSLabel>().ToggleFpsLabel(x);
        });


        commandList = new List<object>
        {
            KILL_ALL,
            ADD_MONEY,
            HELP,
            SET_WAVE,
            GODMODE,
            STAMINA,
            ZOMBIE_LIST,
            ZOMBIE_SPAWN_1,
            ZOMBIE_SPAWN_2,
            ZOMBIE_SPAWN_3,
            ZOMBIE_SPAWN_4,
            SPAWN_NAOD,
            DOOR_INFO,
            DOOR_OPEN,
            MEDIC_LIST,
            MEDIC_SPAWN,
            MEDIC_KILL_ALL,
            AMMO_LIST,
            AMMO_SPAWN,
            AMMO_KILL_ALL,
            CHEAT,
            FPS_SHOW
        };

    }
    public void OpenDoor(int index)
    {
        try
        {
            GameObject.FindGameObjectsWithTag("KeyPadEG")[index].GetComponent<KeyPadController>().OpenDoor();
        }
        catch
        {
            GameObject.FindGameObjectsWithTag("KeyPadEG")[index].GetComponent<LapTopScript>().OpenDoor();
        }        
    }
    public void OnReturn()
    {
        if (showConsole)
        {
            HandleInput();
            input = "";
        }
    }
    public void OnTab()
    {
        if (showConsole)
        {
            input = tippCommands[0];
        }
    }

    public void OnToggleDebug()
    {
        showConsole = !showConsole;
        if (showConsole)
        {
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().inUI = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().inUI = false;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        
    }
    private void OnGUI()
    {
        if (!showConsole) return;
        float y = 0f;
        if (showAmmo)
        {
            GameObject[] AmmoObjects = GameObject.FindGameObjectsWithTag("Ammo");
            GUI.Box(new Rect(0, y, Screen.width, 100), "");

            Rect viewport = new Rect(0, 0, Screen.width - 30f, 20 * (AmmoObjects.Length + 1));

            scroll = GUI.BeginScrollView(new Rect(0, y + 5f, Screen.width, 90), scroll, viewport);

            string label = $"<index - name - ammo amount - spawnpoint>";
            Rect labelRect = new Rect(5, 20 * 0, viewport.width - 100, 20);
            GUI.Label(labelRect, label);
            for (int c = 0; c < AmmoObjects.Length; c++)
            {
                try
                {
                    label = $"{c} - {AmmoObjects[c].name} - {AmmoObjects[c].GetComponent<Ammunition>().GetAmmoAmount()} - {AmmoObjects[c].GetComponent<Ammunition>().GetSpawnPoint()}";
                    labelRect = new Rect(5, 20 * (c + 1), viewport.width - 100, 20);
                    GUI.Label(labelRect, label);
                }
                catch
                {
                }
            }
            GUI.EndScrollView();
            y += 100;
        }
        else if (showMedic)
        {
            GameObject[] MedicObjects = GameObject.FindGameObjectsWithTag("MedicKit");
            GUI.Box(new Rect(0, y, Screen.width, 100), "");

            Rect viewport = new Rect(0, 0, Screen.width - 30f, 20 * (MedicObjects.Length + 1));

            scroll = GUI.BeginScrollView(new Rect(0, y + 5f, Screen.width, 90), scroll, viewport);


            string label = $"<index - name - health amount - spawnpoint>";
            Rect labelRect = new Rect(5, 20 * 0, viewport.width - 100, 20);
            GUI.Label(labelRect, label);
            for (int c = 0; c < MedicObjects.Length; c++)
            {
                try
                {
                    label = $"{c} - {MedicObjects[c].name} - {MedicObjects[c].GetComponent<MedicKit>().GetHealthAmount()} - {MedicObjects[c].GetComponent<MedicKit>().GetSpawnPoint()}";
                    labelRect = new Rect(5, 20 * (c + 1), viewport.width - 100, 20);
                    GUI.Label(labelRect, label);
                }
                catch
                {
                }
            }
            GUI.EndScrollView();

            y += 100;
        }
        else if (showZombies)
        {
            GameObject[] ZombieObjects = GameObject.FindGameObjectsWithTag("Zombie");
            GameObject[] ZombieEndObjects = GameObject.FindGameObjectsWithTag("ZombieEnd");
            GUI.Box(new Rect(0, y, Screen.width, 100), "");

            Rect viewport = new Rect(0, 0, Screen.width - 30f, 20 * (ZombieObjects.Length + 1 + ZombieEndObjects.Length));

            scroll = GUI.BeginScrollView(new Rect(0, y + 5f, Screen.width, 90), scroll, viewport);

            string label = $"<index - name - health - spawnpoint>";
            Rect labelRect = new Rect(5, 20 * 0, viewport.width - 100, 20);
            GUI.Label(labelRect, label);
            for (int c = 0; c < ZombieObjects.Length; c++)
            {
                try{
                    label = $"{c} - {ZombieObjects[c].name} - {ZombieObjects[c].GetComponent<ZombieController>().health} - {ZombieObjects[c].GetComponent<ZombieController>().GetSpawnPoint()}";
                    labelRect = new Rect(5, 20 * (c + 1), viewport.width - 100, 20);
                    GUI.Label(labelRect, label);
                }
                catch
                {
                }                
            }           
            if (ZombieEndObjects.Length > 0)
            {
                for (int c = 0; c < ZombieEndObjects.Length; c++)
                {
                    try
                    {
                        label = $"{ZombieObjects.Length + c} - {ZombieEndObjects[c].name} - {ZombieEndObjects[c].GetComponent<EndBossController>().health}";
                        labelRect = new Rect(5, 20 * (ZombieObjects.Length + c), viewport.width - 100, 20);
                        GUI.Label(labelRect, label);
                    }
                    catch
                    {
                    }
                }
            }            
            
            GUI.EndScrollView();

            y += 100;
        }
        else if (showCodes)
        {
            GameObject[] KeypadObjects = GameObject.FindGameObjectsWithTag("KeyPadEG");
            GUI.Box(new Rect(0, y, Screen.width, 100), "");

            Rect viewport = new Rect(0, 0, Screen.width - 30f, 20 * (KeypadObjects.Length+1));

            scroll = GUI.BeginScrollView(new Rect(0, y + 5f, Screen.width, 90), scroll, viewport);
            
            
            string label = $"<index - name - code>";
            Rect labelRect = new Rect(5, 20 * 0, viewport.width - 100, 20);
            GUI.Label(labelRect, label);
            for (int c = 0; c < KeypadObjects.Length; c++)
            {
                string code = "";
                try
                {
                    code = KeypadObjects[c].GetComponent<KeyPadController>().GetCode();
                }
                catch
                {
                    code = KeypadObjects[c].GetComponent<LapTopScript>().GetCode();
                }
                label = $"{c} - {KeypadObjects[c].name} - {code}";
                labelRect = new Rect(5, 20 * (c+1), viewport.width - 100, 20);
                GUI.Label(labelRect, label);
            }

            GUI.EndScrollView();

            y += 100;
        }
        else if (showHelp)
        {
            GUI.Box(new Rect(0, y, Screen.width, 100), "");

            Rect viewport = new Rect(0, 0, Screen.width - 30f, 20 * (commandList.Count+1));

            scroll = GUI.BeginScrollView(new Rect(0, y + 5f, Screen.width, 90), scroll, viewport);
            string label = $"<format - description>";
            Rect labelRect = new Rect(5, 20 * 0, viewport.width - 100, 20);
            GUI.Label(labelRect, label);
            for (int c = 0; c < commandList.Count; c++)
            {
                DebugCommandBase command = commandList[c] as DebugCommandBase;

                label = $"{command.commandFormat} - {command.commandDescription}";
                labelRect = new Rect(5, 20 * (c+1), viewport.width - 100, 20);
                GUI.Label(labelRect, label);
            }
            GUI.EndScrollView();
            y += 100;
        }
        HandleTipps(y+30f);

        GUI.Box(new Rect(0, y, Screen.width, 30), "");
        GUI.SetNextControlName("console");
        input = GUI.TextField(new Rect(10f, y + 5f, Screen.width - 20f, 20f), input);
        y += 30f;
        TextEditor te = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
        te.MoveTextEnd();
        //GUI.Box(new Rect(0, y, Screen.width, 20* commandCounter), "");
        GUI.FocusControl("console");
    }
    private void HandleTipps(float y)
    {
        int commandCounter = 0;
        tippCommands = new List<string>();
        for (int i = 0; i < commandList.Count; i++)
        {
            DebugCommandBase command = commandList[i] as DebugCommandBase;
            if (input != null && command.commandId.StartsWith(input))
            {
                commandCounter += 1;
                tippCommands.Add($"{command.commandId}");
                string label = $"{command.commandFormat}";
                Rect labelRect = new Rect(10, y, Screen.width - 30f, 30);
                y += 15;
                GUI.Label(labelRect, label);
            }
        }
    }
    private void HandleInput()
    {
        string[] properties = input.Split(' ');
        for(int i = 0; i < commandList.Count; i++)
        {            
            DebugCommandBase commandBase = commandList[i] as DebugCommandBase;
            if (input.Contains(commandBase.commandId))
            {
                if (commandList[i] is DebugCommand)
                {
                    (commandList[i] as DebugCommand).Invoke();
                }
                else if (commandList[i] is DebugCommand<int>)
                {
                    (commandList[i] as DebugCommand<int>).Invoke(int.Parse(properties[1]));
                }
            }
        }
    }
}
