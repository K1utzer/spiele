using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieController : MonoBehaviour
{
    private NavMeshAgent zombieNavMesh;
    private Transform player;
    private GameObject playerObject;
    //public GameManager gameManager;
    public float damage = 20f;
    public float health = 100f;
    public float attackDelay = 1f;
    public int money_worth;
    public Transform bloodParticles;

    public GameObject hitSounds;
    public AudioSource hitSound;
    public GameObject deadShounds;

    private float TimeOfLastAttack = 0;
    private bool hasReachedPlayer = false;
    public bool isDead = false;
    private float TimeOfDeath = 0f;
    private float deathTime = 2.5f;
    public GameObject ZombieSounds;
    private float TimeForSound = 5f;
    private float StartTimeSound;
	[SerializeField]
	private GameObject[] dropItems;
	private bool OnHead = false;
	private string SpawnPoint = "";
    // Start is called before the first frame update
    void Start()
    {
        
        try
        {
            damage -= 10 / Difficulty.difficulty;
            health -= 20 / Difficulty.difficulty;
            money_worth += 20 / Difficulty.difficulty;
        }
        catch { }
        
        StartTimeSound = Random.Range(0, 5);
        playerObject = GameObject.FindGameObjectWithTag("Player");
        player = playerObject.transform;
        zombieNavMesh = this.GetComponent<NavMeshAgent>();
    }
	public void SetSpawnPoint(string spawnpoint)
	{
		SpawnPoint = spawnpoint;
	}
	public string GetSpawnPoint()
	{
		return SpawnPoint;
	}
    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            if (Time.time >= StartTimeSound + TimeForSound)
            {
                AudioSource[] zombieSounds = ZombieSounds.GetComponentsInChildren<AudioSource>();
                zombieSounds[Random.Range(0, zombieSounds.Length)].Play();
                StartTimeSound = Time.time;

                TimeForSound = Random.Range(2, 15);
            }

            float y = player.position.y - 1.55f;
            try
            {
                zombieNavMesh.SetDestination(new Vector3(player.position.x, y, player.position.z));
            }
            catch
            {
            }
            
            //transform.LookAt(player);
            Vector3 direction = player.position - transform.position;
            direction.Set(direction.x, 0, direction.z);
            Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
            transform.rotation = rotation;

            float distanceToTarget = Vector3.Distance(player.position, transform.position);
            if (distanceToTarget <= zombieNavMesh.stoppingDistance + 1 || OnHead)
            {
                AttackTarget();
            }
            else
            {
                this.GetComponent<Animator>().ResetTrigger("attack");
                hasReachedPlayer = false;
            }
        }        
    }
    private void SetDeathPhys()
    {
        this.GetComponent<Rigidbody>().useGravity = false;
        this.GetComponent<Rigidbody>().isKinematic = true;
        this.GetComponent<CapsuleCollider>().enabled = false;        
    }
    public bool TakeDamage(float damageAmount, float bulletType)
    {
        Instantiate(hitSound, transform).Play();
        
        GameObject Soundobject = Instantiate(hitSounds, transform);
        AudioSource[] audios = Soundobject.GetComponentsInChildren<AudioSource>();
        audios[Random.Range(0, audios.Length)].Play();
        Destroy(Soundobject, 1f);
        health -= damageAmount;
        if(health <= 0)
        {
            playerObject.GetComponent<PlayerMotor>().AddMoney(money_worth);
            GameObject Deadobject = Instantiate(deadShounds, transform);
            AudioSource[] audios2 = Deadobject.GetComponentsInChildren<AudioSource>();
            audios2[Random.Range(0, audios2.Length)].Play();
            isDead = true;
            TimeOfDeath = Time.time;
            this.GetComponent<Animator>().SetTrigger("dead");
            this.GetComponent<CapsuleCollider>().enabled = false;
            this.GetComponent<NavMeshAgent>().enabled = false;
            this.GetComponent<Animator>().ResetTrigger("attack");
            Destroy(Soundobject, 3f);          
			GameObject.FindGameObjectWithTag("BobTalk").GetComponent<ParamPlayerInteract>().AddKilledZombie();
            int random = Random.Range(0, 50);

            if (random >= 0 && random <= (4/Difficulty.difficulty))
			{
				GameObject dropItem = dropItems[Random.Range(0, 3)];
				Instantiate(dropItem, transform.position, dropItem.transform.rotation);
			}else if(random == 5)
            {
                GameObject dropItem = dropItems[Random.Range(3, dropItems.Length)];
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
                {
                    Instantiate(dropItem, new Vector3(transform.position.x, hit.transform.position.y + 1f, transform.position.z), dropItem.transform.rotation);
                }
            }	
            if(bulletType == 3)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject, 7f);
            }
            
            return true;
        }
        return false;
    }
	private void AttackTarget()
    {
        if (!hasReachedPlayer)
        {
            hasReachedPlayer = true;
            TimeOfLastAttack = Time.time;
        }        
        if (Time.time >= TimeOfLastAttack + attackDelay)
        {
            TimeOfLastAttack = Time.time;
            this.GetComponent<Animator>().SetTrigger("attack");
            playerObject.GetComponent<PlayerHealth>().TakeDamage(damage);
            //this.TakeDamage(60);
        }        
    }
	private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject == playerObject)
        {
            OnHead = true;
        }
    }
	private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == playerObject)
        {            
            OnHead = false;
        }
        
    }
	
    /*private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject == playerObject)
        {
            AttackTarget();
            //this.gameObject.GetComponent<Animations>().state = 1;
            //DestroyGameObject(this.gameObject);
            //gameManager.enemiesAlive--;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        AttackTarget();

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == playerObject)
        {            
            this.GetComponent<Animator>().ResetTrigger("attack");
            hasReachedPlayer = false;
        }
        
    }*/
    
}
