using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EndBossController : MonoBehaviour
{
    private NavMeshAgent zombieNavMesh;
    private Transform player;
    private GameObject playerObject;
    //public GameManager gameManager;
    public float damage = 20f;
    public float health = 100f;
    public float attackDelay = 1f;
    private float TimeOfLastAttack = 0;
    private bool hasReachedPlayer = false;
    public bool isDead = false;
    private float TimeOfDeath = 0f;
    private float deathTime = 2.5f;
    public int moneyWorth;
    private bool OnHead = false;
    // Start is called before the first frame update
    void Start()
    {
        try
        {
            damage -= 10 / Difficulty.difficulty;
            health -= 20 / Difficulty.difficulty;
            moneyWorth += 20 / Difficulty.difficulty;
        }
        catch { }
        playerObject = GameObject.FindGameObjectWithTag("Player"); ;
        player = playerObject.transform;
        zombieNavMesh = this.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!isDead)
        {
            try
            {
                zombieNavMesh.SetDestination(player.position);
            }
            catch
            {
                zombieNavMesh = this.GetComponent<NavMeshAgent>();
            }
            
            //transform.LookAt(player);
            Vector3 direction = player.position - transform.position;
            direction.Set(direction.x, 0, direction.z);
            Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
            transform.rotation = rotation;

            float distanceToTarget = Vector3.Distance(player.position, transform.position);
            if (distanceToTarget <= zombieNavMesh.stoppingDistance || OnHead)
            {
                AttackTarget();
            }
            else
            {
                this.GetComponent<Animator>().ResetTrigger("attack");
                hasReachedPlayer = false;
            }
        }
        
    }
    public bool TakeDamage(float damageAmount)
    {
        health -= damageAmount;
        if(health <= 0)
        {
            isDead = true;
            TimeOfDeath = Time.time;
            foreach (CapsuleCollider c in GetComponentsInChildren<CapsuleCollider>())
            {
                c.enabled = false;
            }
            this.GetComponent<NavMeshAgent>().enabled = false;
            this.GetComponent<Animator>().SetTrigger("dead");
            this.GetComponent<Animator>().ResetTrigger("attack");
            StartCoroutine(DestroyGameObject(this.gameObject));
            playerObject.GetComponent<PlayerMotor>().AddMoney(moneyWorth);
            return true;
        }
        return false;
    }
    IEnumerator DestroyGameObject(GameObject gameObject)
    {
        yield return new WaitForSeconds(7);
        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject == playerObject)
        {
            OnHead = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == playerObject)
        {
            OnHead = false;
        }

    }
    /*private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject == playerObject)
        {
            AttackTarget();
            //this.gameObject.GetComponent<Animations>().state = 1;
            //DestroyGameObject(this.gameObject);
            //gameManager.enemiesAlive--;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        AttackTarget();

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == playerObject)
        {            
            this.GetComponent<Animator>().ResetTrigger("attack");
            hasReachedPlayer = false;
        }
        
    }*/
    private void AttackTarget()
    {
        if (!hasReachedPlayer)
        {
            hasReachedPlayer = true;
            TimeOfLastAttack = Time.time;
            this.GetComponent<Animator>().SetTrigger("attack");
        }
        
        if (Time.time >= TimeOfLastAttack + attackDelay)
        {
            this.GetComponent<Animator>().SetTrigger("attack");
            TimeOfLastAttack = Time.time;
            //  this.GetComponent<Animator>().SetTrigger("attack");
            playerObject.GetComponent<PlayerHealth>().TakeDamage(damage);
        }        
    }
}
