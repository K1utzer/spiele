﻿using UnityEngine;
using System.Collections;

public class Animations : MonoBehaviour {

	public GameObject TargetChar;
    public AnimationClip Idle;
	public AnimationClip AttackAnim;
	public AnimationClip WalkZombieAnim;
    public AnimationClip GetHitAnim;
	public AnimationClip DieAnim;


    public int state = 0;
	//private MonoBehaviour CharControl;


	// Use this for initialization
	void Start () {

        //	CharControl = TargetChar.GetComponent<ThirdPersonController>();
        state = 0;
	
	}
    private void Death()
    {
        state = 2;
        TargetChar.GetComponent<Animation>().PlayQueued(DieAnim.name);
        Destroy(this.gameObject, 10f);
    }
	// Update is called once per frame
	void Update () {
        if (state == 0)
        {
            TargetChar.GetComponent<Animation>().PlayQueued(WalkZombieAnim.name);
        }else if (state == 1)
        {
            TargetChar.GetComponent<Animation>().Play(AttackAnim.name);
        }
        else if (state == 2)
        {
            Death();
        }

    }
}
