using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Difficulty
{
	public static int difficulty {get; set;}
	public static int health {get;set;}
	public static int wave {get; set;}
	public static int lives {get; set;}
}
