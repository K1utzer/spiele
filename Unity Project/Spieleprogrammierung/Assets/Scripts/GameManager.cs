using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int enemiesAlive = 0;

    public int round= 0;

    [Header("Spawnpoints Zombies")]
    public Transform spawnPointZombiesEG;
    public Transform spawnPointZombies1OG;
    public Transform spawnPointZombiesDACH;

    [Header("Spawnpoints Objects")]
    public Transform spawnPointObjectEG;
    public Transform spawnPointObjectOG;
    public Transform spawnPointObjectDACH;
    //private Transform[] spawnPointObjectsEG;
    //private Transform[] spawnPointObjectsOG;
    //private Transform[] spawnPointObjectsDACH;

    public TextMeshProUGUI waveText;

    public GameObject[] enemyPrefab;

    public GameObject endBossPrefab;
    public Transform endBossSpawn;
    public Animator endBossSpawnAnim;

    public GameObject pauseMenu;
    public GameObject gameOverMenu;

    public GameObject medicKit;
    public GameObject ammunitionLight;
    public GameObject ammunition;
    private float NextRoundToSpawnMedic;
    private float NextRoundToSpawnAmmo;

    public GameObject laptopUI;

    private bool newWaveReady = true;

    public bool inUI = false;


    public int currentLevel = 0; // 0 EG, 1 1.OG, 2 Dach
    public int Unlocked_level = 0;
    private Transform Player;

    
    private int[] usedSpawnPoints; // 0 ist leer, 1 ist medic kit, 2 munition

    [Header("KeyPadController")]
    public KeyPadController[] keyPadControllerNumbers;
    public KeyPadController[] keyPadControllerColors;
    [SerializeField]
    private TextMeshProUGUI[] keyPadTexts;
    private int[] RandomRoundSpawnNumber;
    private int[] RandomRoundColorSpawnNumber;

    public GameObject ui;
    [SerializeField]
    private TextMeshProUGUI HighScoreText;
	[SerializeField]
    private GameObject MainMenuButton;
	[SerializeField]
    private GameObject RestartButton;
	[SerializeField]
    private GameObject QuitButton;
	[SerializeField]
    private GameObject LoadingText;
	
	private AsyncOperation sceneToLoad;
	private int lives = 1;
    public bool newWaveSet = false;

    //public TextMeshProUGUI roundNum;
    //public TextMeshProUGUI roundsSurvived;
    // Start is called before the first frame update
    void Start()
    {
		round = Difficulty.wave;
		lives =	Difficulty.lives;
		Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        AudioListener.volume = PlayerPrefs.GetFloat("Volume");
		
        usedSpawnPoints = new int[spawnPointObjectEG.GetComponentsInChildren<Transform>().Length+spawnPointObjectOG.GetComponentsInChildren<Transform>().Length+spawnPointObjectDACH.GetComponentsInChildren<Transform>().Length];
        NextRoundToSpawnMedic = Random.Range(8, 12); //zwieschen runde 8 und 12
        NextRoundToSpawnAmmo = Random.Range(4, 12); //zwieschen runde 4 und 12
        //Debug.Log(NextRoundToSpawnMedic);
        waveText.text = $"{round}";
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        //spawnPointObjectsEG = spawnPointObjectEG.GetComponentsInChildren<Transform>();
        var rand = new System.Random();
        RandomRoundSpawnNumber = new int[keyPadControllerNumbers.Length];
        int randNum = rand.Next(2, 15); ;
        for (int i = 0; i < RandomRoundSpawnNumber.Length; i++)
        {
            RandomRoundSpawnNumber[i] = randNum;
            Debug.Log("KeyPad Number Tipp Spawn round: "+RandomRoundSpawnNumber[i]);
        }
        RandomRoundColorSpawnNumber = new int[keyPadControllerColors.Length];
        for (int i = 0; i < RandomRoundColorSpawnNumber.Length; i++)
        {
            RandomRoundColorSpawnNumber[i] = randNum;
            Debug.Log("KeyPad Color Tipp Spawn round: " + RandomRoundColorSpawnNumber[i]);
        }
        if (RandomRoundColorSpawnNumber[0] >= this.round)
        {
            foreach (TextMeshProUGUI text in keyPadTexts)
            {
                int leftRounds = (RandomRoundColorSpawnNumber[0] - this.round) >= 0 ? RandomRoundColorSpawnNumber[0] - this.round : 0;
                text.text = $"{leftRounds}";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        float player_y = Player.position.y;
        if(player_y >= 10)
        {
            currentLevel = 2;
            
        }
        else if (player_y >= 5.8)
        {
            currentLevel = 1;
        }
        else
        {
            currentLevel = 0;
        }
        enemiesAlive = GameObject.FindGameObjectsWithTag("Zombie").Length;
        if (enemiesAlive <= 0 && newWaveReady)
        {
            newWaveReady = false;
            enemiesAlive = 0;
            round++;
            if(NextRoundToSpawnMedic <= round)
            {
                PlaceMedicKit();
                NextRoundToSpawnMedic = Random.Range((round + 4), (round + 8));
            }
            if (NextRoundToSpawnAmmo <= round)
            {
                PlaceAmmunition();
                NextRoundToSpawnAmmo = Random.Range((round + 2), (round + 4));
            }
            endBossSpawnAnim.SetTrigger("roundend");
            StartCoroutine(WaitForNextWave());
            //roundNum.text = "Round: " + round.ToString();
        }
    }
    public void SetWave(int round)
    {
        enemiesAlive = 0;
        newWaveReady = false;
        newWaveSet = true;
        KillAllZombies(null);
        this.round = round;
        if (NextRoundToSpawnMedic <= this.round)
        {
            PlaceMedicKit();
            NextRoundToSpawnMedic = Random.Range((this.round + 4), (this.round + 8));
        }
        if (NextRoundToSpawnAmmo <= this.round)
        {
            PlaceAmmunition();
            NextRoundToSpawnAmmo = Random.Range((this.round + 2), (this.round + 4));
        }
        endBossSpawnAnim.SetTrigger("roundend");
        
        StartCoroutine(WaitForNextWave());
    }
    public void SpawnMedicKit(int amount)
    {
        Transform[] spawnpoints = GetAvailableObjectSpawnPoints();
        int checkAmount = 0;
        for(int c = 0; c < spawnpoints.Length; c++)
        {
            if(usedSpawnPoints[c] <= 0)
            {
                checkAmount += 1;
            }
        }
        if(checkAmount < amount)
        {
            amount = checkAmount;
        }
        int spawnPoint;
        bool isOk = false;
        do
        {
            spawnPoint = Random.Range(0, spawnpoints.Length);
            if (usedSpawnPoints[spawnPoint] <= 0)
            {
                GameObject tmpMedic = Instantiate(medicKit, spawnpoints[spawnPoint].position, medicKit.transform.rotation);
                tmpMedic.GetComponent<MedicKit>().SetSpawnPoint(spawnPoint);
                usedSpawnPoints[spawnPoint] = 1;
                amount -= 1;
            }
            if (amount <= 0) isOk = true;
        } while (!isOk);
    }
    private void PlaceMedicKit()
    {
        if (usedSpawnPoints.Count(n => n == 1) >= 5) return; // schon zuviele gespawnte medic kits
        Transform[] spawnpoints = GetAvailableObjectSpawnPoints();
        int spawnPoint;
        int amount = Random.Range(1, 7 - usedSpawnPoints.Count(n => n == 1));

        int checkAmount = 0;
        for (int c = 0; c < spawnpoints.Length; c++)
        {
            if (usedSpawnPoints[c] <= 0)
            {
                checkAmount += 1;
            }
        }
        if (checkAmount < amount)
        {
            amount = checkAmount;
        }
        bool isOk = false;
        do
        {
            spawnPoint = Random.Range(0, spawnpoints.Length);
            if (usedSpawnPoints[spawnPoint] <= 0)
            {
                GameObject tmpMedic = Instantiate(medicKit, spawnpoints[spawnPoint].position, medicKit.transform.rotation);
                tmpMedic.GetComponent<MedicKit>().SetSpawnPoint(spawnPoint);
                usedSpawnPoints[spawnPoint] = 1;
                amount -= 1;
            }
            if (amount <= 0) isOk = true;
        } while (!isOk);
    }
    public void SpawnAmmunition(int amount)
    {
        Transform[] spawnpoints = GetAvailableObjectSpawnPoints();
        int spawnPoint;

        int amount1 = amount / 2;
        int amount2 = amount / 2;

        int checkAmount = 0;
        for (int c = 0; c < spawnpoints.Length; c++)
        {
            if (usedSpawnPoints[c] <= 0)
            {
                checkAmount += 1;
            }
        }
        amount = amount1 + amount2;
        if (checkAmount < amount)
        {
            amount = checkAmount;
            amount1 = checkAmount/2;
            amount2 = checkAmount/2;
        }
        bool isOk = false;
        do
        {
            spawnPoint = Random.Range(0, spawnpoints.Length);
            if (usedSpawnPoints[spawnPoint] <= 0)
            {
                GameObject tmpAmmo;
                if (amount1 > 0)
                {
                    tmpAmmo = Instantiate(ammunition, spawnpoints[spawnPoint].position, ammunition.transform.rotation);
                    amount1 -= 1;
                }
                else
                {
                    tmpAmmo = Instantiate(ammunitionLight, spawnpoints[spawnPoint].position, ammunitionLight.transform.rotation);
                    amount2 -= 1;
                }

                tmpAmmo.GetComponent<Ammunition>().SetSpawnPoint(spawnPoint);
                usedSpawnPoints[spawnPoint] = 2;
                amount -= 1;
            }
            if (amount <= 0) isOk = true;
        } while (!isOk);
    }
    private void PlaceAmmunition()
    {
        if (usedSpawnPoints.Count(n => n == 2) >= 5) return; // schon zuviele gespawnte medic kits
        Transform[] spawnpoints = GetAvailableObjectSpawnPoints();
        int spawnPoint;
        int amount = 5;
        int amount1 = Random.Range(1, 5);
        int amount2 = Random.Range(1, 5-amount1);

        int checkAmount = 0;
        for (int c = 0; c < spawnpoints.Length; c++)
        {
            if (usedSpawnPoints[c] <= 0)
            {
                checkAmount += 1;
            }
        }
        amount = amount1 + amount2;
        if (checkAmount < amount)
        {
            amount = checkAmount;
            amount1 = 0;
            amount2 = checkAmount;
        }
        bool isOk = false;
        do
        {
            spawnPoint = Random.Range(0, spawnpoints.Length);
            if (usedSpawnPoints[spawnPoint] <= 0)
            {
                GameObject tmpAmmo;
                if (amount1 > 0)
                {
                    tmpAmmo = Instantiate(ammunition, spawnpoints[spawnPoint].position, ammunition.transform.rotation);
                    amount1 -= 1;
                }
                else
                {
                    tmpAmmo = Instantiate(ammunitionLight, spawnpoints[spawnPoint].position, ammunitionLight.transform.rotation);
                    amount2 -= 1;
                }
                
                tmpAmmo.GetComponent<Ammunition>().SetSpawnPoint(spawnPoint);
                usedSpawnPoints[spawnPoint] = 2;
                amount -= 1;
            }
            if (amount <= 0) isOk = true;
        } while (!isOk);
    }
    private Transform[] GetAvailableObjectSpawnPoints()
    {
        Transform[] spawnpoints;
		if (Unlocked_level <= 0)
        {
            spawnpoints = FindComponentsInChildWithTag(spawnPointObjectEG, "Spawn");
        }
        else if (Unlocked_level == 1)
        {
            Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointObjectEG, "Spawn");
            Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointObjectOG, "Spawn");

            spawnpoints = eg_spawns.Concat(og1_spawns).ToArray();
        }
        else
        {
            Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointObjectEG, "Spawn");
            Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointObjectOG, "Spawn");
            Transform[] dach_spawns = FindComponentsInChildWithTag(spawnPointObjectDACH, "Spawn");
            spawnpoints = eg_spawns.Concat(og1_spawns).ToArray().Concat(dach_spawns).ToArray();
        }
        return spawnpoints;
    }
    public void FreeSpawnPoint(int index)
    {
        usedSpawnPoints[index] = 0;
    }
    public void KillAllMedicKits()
    {
        for(int c = 0; c < usedSpawnPoints.Length; c++)
        {
            if(usedSpawnPoints[c] == 1)
            {
                usedSpawnPoints[c] = 0;
            }
        }
        foreach(GameObject kit in GameObject.FindGameObjectsWithTag("MedicKit"))
        {
            Destroy(kit);
        }
    }
    public void KillAllAmmo()
    {
        for (int c = 0; c < usedSpawnPoints.Length; c++)
        {
            if (usedSpawnPoints[c] == 2)
            {
                usedSpawnPoints[c] = 0;
            }
        }
        foreach (GameObject kit in GameObject.FindGameObjectsWithTag("Ammo"))
        {
            Destroy(kit);
        }
    }
    public void SetUnlockedLevel(int level)
    {
        if(level > Unlocked_level)
        {
            Unlocked_level = level;
        }        
    }
    IEnumerator WaitForNextWave()
    {
        waveText.text = $"{round}";
        if (RandomRoundColorSpawnNumber[0] >= this.round)
        {
            foreach (TextMeshProUGUI text in keyPadTexts)
            {
                int leftRounds = (RandomRoundColorSpawnNumber[0] - this.round) >= 0 ? RandomRoundColorSpawnNumber[0] - this.round : 0;
                text.text = $"{leftRounds}";
            }
        }
        CheckAndSaveHighScore();
        yield return new WaitForSeconds(10);
        NextWave();
    }
    public void NextWave()
    {
        StartCoroutine(SpawnWaves());
        if (round >= 10 && round%10 == 0)
        {
            Transform[] spawnpoints;
            if (Unlocked_level <= 0)
            {
                spawnpoints = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
            }
            else if (Unlocked_level == 1)
            {
                Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
                Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointZombies1OG, "Spawn");

                spawnpoints = eg_spawns.Concat(og1_spawns).ToArray();
            }
            else
            {
                Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
                Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointZombies1OG, "Spawn");
                Transform[] dach_spawns = FindComponentsInChildWithTag(spawnPointZombiesDACH, "Spawn");
                spawnpoints = eg_spawns.Concat(og1_spawns).ToArray().Concat(dach_spawns).ToArray();
            }

            Transform spawnPoint = spawnpoints[Random.Range(0, spawnpoints.Length)];

            float tmp_count_bosses = round / 10;
            int count_bosses = System.Int32.Parse(tmp_count_bosses.ToString("0"));
            for (int c = 0; c < count_bosses; c++)
            {
                GameObject enemySpawned = Instantiate(endBossPrefab, spawnPoint.position, spawnPoint.rotation);
                enemiesAlive++;
            }
        }
        for (int i = 0; i < keyPadControllerNumbers.Length; i++)
        {
            if (RandomRoundSpawnNumber[i] == round)
            {
                keyPadControllerNumbers[i].SetNumberTippInMap();
            }
        }
        for (int i = 0; i < keyPadControllerColors.Length; i++)
        {
            if (RandomRoundColorSpawnNumber[i] == round)
            {
                keyPadControllerColors[i].SetColorTippInMap();
            }
        }          
        newWaveReady = true;
    }
    IEnumerator SpawnWaves()
    {
        foreach (GameObject enemyPrefab in DecideEnemys())
        {
            if (newWaveSet)
            {
                newWaveSet = false;
                yield break;
            }
            Transform[] spawnpoints;
            if (Unlocked_level <= 0)
            {
                spawnpoints = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
            }
            else if (Unlocked_level == 1)
            {
                Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
                Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointZombies1OG, "Spawn");

                spawnpoints = eg_spawns.Concat(og1_spawns).ToArray();
            }
            else
            {
                Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
                Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointZombies1OG, "Spawn");
                Transform[] dach_spawns = FindComponentsInChildWithTag(spawnPointZombiesDACH, "Spawn");
                spawnpoints = eg_spawns.Concat(og1_spawns).ToArray().Concat(dach_spawns).ToArray();
            }

                Transform spawnPoint = spawnpoints[Random.Range(0, spawnpoints.Length)];

                GameObject enemySpawned = Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity);			
                enemySpawned.SetActive(true);
			    enemySpawned.GetComponent<ZombieController>().SetSpawnPoint(spawnPoint.gameObject.name);
                enemiesAlive++;
                yield return new WaitForSeconds(1.5f);
                //enemySpawned.GetComponent<ZombieController>().gameManager = GetComponent<GameManager>();            
        }
    }
    public void SpawnZombie(int type, int amount)
    {
        if (type > enemyPrefab.Length) return;
        Transform[] spawnpoints;
        if (Unlocked_level <= 0)
        {
            spawnpoints = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
        }
        else if (Unlocked_level == 1)
        {
            Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
            Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointZombies1OG, "Spawn");

            spawnpoints = eg_spawns.Concat(og1_spawns).ToArray();
        }
        else
        {
            Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
            Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointZombies1OG, "Spawn");
            Transform[] dach_spawns = FindComponentsInChildWithTag(spawnPointZombiesDACH, "Spawn");
            spawnpoints = eg_spawns.Concat(og1_spawns).ToArray().Concat(dach_spawns).ToArray();
        }
        for(int c = 0; c < amount; c++)
        {
            Transform spawnPoint = spawnpoints[Random.Range(0, spawnpoints.Length)];
            
            GameObject enemySpawned = Instantiate(enemyPrefab[type], spawnPoint.position, Quaternion.identity);
            //enemySpawned.GetComponent<ZombieController>().gameManager = GetComponent<GameManager>();
            enemySpawned.SetActive(true);
			enemySpawned.GetComponent<ZombieController>().SetSpawnPoint(spawnPoint.gameObject.name);
            enemiesAlive++;
        }
    }
    public void SpawnNaod(int amount)
    {
        Transform[] spawnpoints;
        if (Unlocked_level <= 0)
        {
            spawnpoints = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
        }
        else if (Unlocked_level == 1)
        {
            Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
            Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointZombies1OG, "Spawn");

            spawnpoints = eg_spawns.Concat(og1_spawns).ToArray();
        }
        else
        {
            Transform[] eg_spawns = FindComponentsInChildWithTag(spawnPointZombiesEG, "Spawn");
            Transform[] og1_spawns = FindComponentsInChildWithTag(spawnPointZombies1OG, "Spawn");
            Transform[] dach_spawns = FindComponentsInChildWithTag(spawnPointZombiesDACH, "Spawn");
            spawnpoints = eg_spawns.Concat(og1_spawns).ToArray().Concat(dach_spawns).ToArray();
        }

        Transform spawnPoint = spawnpoints[Random.Range(0, spawnpoints.Length)];
        for (int c = 0; c < amount; c++)
        {
            GameObject enemySpawned = Instantiate(endBossPrefab, spawnPoint.position, endBossPrefab.transform.rotation);
            enemiesAlive++;
        }
        
    }
    public void CheckAndSaveHighScore()
    {	
		HighScoreText.text = "HighScore\n";
		string[] tmpHighs = new string[]{"Easy","Medium","Hard","Extrem"};
		if(PlayerPrefs.HasKey($"HighScore{Difficulty.difficulty}") && PlayerPrefs.GetInt($"HighScore{Difficulty.difficulty}") < round);
		{
			PlayerPrefs.SetInt($"HighScore{Difficulty.difficulty}", round);
		}
		for(int i = 1; i < tmpHighs.Length+1; i++)
		{
			if(PlayerPrefs.HasKey($"HighScore{i}"))
			{	
				HighScoreText.text += $"{tmpHighs[i-1]}: {PlayerPrefs.GetInt($"HighScore{i}")}\n";
			}else
			{
				HighScoreText.text += $"{tmpHighs[i-1]}: 0\n";
			}
		}
    }
    public void KillAllZombies(GameObject explosion)
    {
        foreach(GameObject zombie in GameObject.FindGameObjectsWithTag("Zombie"))
        {
            Player.GetComponent<PlayerMotor>().AddMoney(15);
            if(explosion != null) Instantiate(explosion, zombie.transform.position, explosion.transform.rotation);
            Destroy(zombie);
        }
        foreach (EndBossController zombie in FindObjectsOfType<EndBossController>())
        {
            Player.GetComponent<PlayerMotor>().AddMoney(15);
            if (explosion != null) Instantiate(explosion, zombie.transform.position, explosion.transform.rotation);
            Destroy(zombie.gameObject);
        }
    }
    public void KillZombie(int index)
    {
        try
        {
            Destroy(GameObject.FindGameObjectsWithTag("Zombie")[index]);
        }
        catch
        {
        }
    }
    public void GameOver()
    {   
		lives -= 1;
		/*if(lives != 0)
		{
			sceneToLoad = SceneManager.LoadSceneAsync("G2 Map");
			sceneToLoad.allowSceneActivation = true;
			Difficulty.wave = round-1;
			Difficulty.lives = lives;
			Time.timeScale = 0;
			return;
		}*/
		sceneToLoad = SceneManager.LoadSceneAsync("G2 Map");
		sceneToLoad.allowSceneActivation = false;
        AudioListener.volume = 0;
        inUI = true;
        gameOverMenu.SetActive(true);
		LoadingText.SetActive(false);
        SetPlayerUI(false);
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        CheckAndSaveHighScore();
    }
	public void RestartButtonPressed()
	{
		MainMenuButton.SetActive(false);
		RestartButton.SetActive(false);
		QuitButton.SetActive(false);
		LoadingText.SetActive(true);
		sceneToLoad.allowSceneActivation = true;
	}
    public GameObject[] DecideEnemys()
    {
        int enemysToSpawn = round * 2;
        if (round < 10)
        {
            GameObject[] enemys = new GameObject[enemysToSpawn];
            for (int i = 0; i < enemysToSpawn; i++)
            {
                enemys[i] = enemyPrefab[0];
            }
            return enemys;
        }
        if (round < 20)
        {
            GameObject[] enemys = new GameObject[enemysToSpawn];
            int anzahlEnemys = enemysToSpawn * 15 / 100;
            for (int i = 0; i < anzahlEnemys; i++)
            {
                enemys[i] = enemyPrefab[1];
            }
            for (int i = anzahlEnemys; i < enemysToSpawn; i++)
            {
                enemys[i] = enemyPrefab[0];
            }
            return enemys;
        }
        if (round < 30)
        {
            GameObject[] enemys = new GameObject[enemysToSpawn];
            int anzahlEnemysEins = enemysToSpawn * 15 / 100;
            int anzahlEnemysZwei = enemysToSpawn * 10 / 100;
            for (int i = 0; i < anzahlEnemysEins; i++)
            {
                enemys[i] = enemyPrefab[1];
            }
            for (int i = anzahlEnemysEins; i < anzahlEnemysEins + anzahlEnemysZwei; i++)
            {
                enemys[i] = enemyPrefab[2];
            }
            for (int i = anzahlEnemysEins + anzahlEnemysZwei; i < enemysToSpawn; i++)
            {
                enemys[i] = enemyPrefab[0];
            }
            System.Array.Reverse(enemys);
            return enemys;
        }
        if (round >= 30)
        {
            GameObject[] enemys = new GameObject[enemysToSpawn];
            int anzahlEnemysEins = enemysToSpawn * 15 / 100;
            int anzahlEnemysZwei = enemysToSpawn * 10 / 100;
            int anzahlEnemysDrei = enemysToSpawn * 5 / 100;
            for (int i = 0; i < anzahlEnemysEins; i++)
            {
                enemys[i] = enemyPrefab[1];
            }
            for (int i = anzahlEnemysEins; i < anzahlEnemysEins + anzahlEnemysZwei; i++)
            {
                enemys[i] = enemyPrefab[2];
            }
            for (int i = anzahlEnemysEins + anzahlEnemysZwei; i < anzahlEnemysEins + anzahlEnemysZwei + anzahlEnemysDrei; i++)
            {
                enemys[i] = enemyPrefab[3];
            }
            for (int i = anzahlEnemysEins + anzahlEnemysZwei + anzahlEnemysDrei; i < enemysToSpawn; i++)
            {
                enemys[i] = enemyPrefab[0];
            }
            System.Array.Reverse(enemys);
            return enemys;
        }
        return null;
    }


    public void Pause()
    {
        if (inUI) return;
        CheckAndSaveHighScore();
        inUI = true;
        pauseMenu.SetActive(true);
        laptopUI.SetActive(false);
        SetPlayerUI(false);
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        AudioListener.volume = 0;
    }
    public void LaptopEnter()
    {
        inUI = true;
        laptopUI.SetActive(true);
        SetPlayerUI(false);
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void UnPause()
    {
        inUI = false;
        pauseMenu.SetActive(false);
        SetPlayerUI(true);
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        AudioListener.volume = PlayerPrefs.GetFloat("Volume");
    }
    private void SetPlayerUI(bool active)
    {
        ui.SetActive(active);
    }
	private Transform[] FindComponentsInChildWithTag(Transform parent, string tag)
    {

        Transform[] t = parent.GetComponentsInChildren<Transform>(true);
		List<Transform> transforms = new List<Transform>();
		
        foreach(Transform tr in t)
        {
            if(tr.tag == tag)
            {
                transforms.Add(tr);
            }
        }
        return transforms.ToArray();
    }
}
