using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomIdleAnim : MonoBehaviour
{
    private AnimationClip[] clips;
    private Animator animator;
    private int randIdle = 0;

    private void Awake()
    {
        //transform.rotation = new Quaternion(transform.rotation.x, 33, transform.rotation.z, transform.rotation.w);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, Random.Range(0,180), transform.eulerAngles.z);
        // Get the animator component
        animator = GetComponent<Animator>();
        // Get all available clips
        clips = animator.runtimeAnimatorController.animationClips;
        
        StartCoroutine(PlayRandomly());
    }
    private IEnumerator PlayRandomly()
    {
        var randClip = clips[0];  
        yield return new WaitForSeconds(randClip.length);
        //animator.applyRootMotion = false;
        while (true)
        {
            randIdle = Random.Range(1, 3);
            randClip = clips[randIdle];
            animator.SetInteger("idle", randIdle);
            yield return new WaitForSeconds(randClip.length);
        }            
    }
}
