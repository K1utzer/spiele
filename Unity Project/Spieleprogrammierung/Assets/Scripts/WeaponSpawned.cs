using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSpawned : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Break());
        
    }

    IEnumerator Break()
    {
        yield return new WaitForSeconds(3);
        Rigidbody rigid = this.GetComponent<Rigidbody>();
        rigid.mass = 0;
        rigid.useGravity = false;
        BoxCollider collider = GetComponent<BoxCollider>();
        collider.enabled = false;
    }
}
