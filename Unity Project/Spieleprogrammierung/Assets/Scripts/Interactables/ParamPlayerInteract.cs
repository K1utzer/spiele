using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ParamPlayerInteract : Interactable
{
    private bool firstInteract = true;
    private int zombiesToKill;
    private int killedZombies = 0;
    private bool gotParams = false;
    private string afterGotText;
    private GameManager gameManager;
    private string param1;
    private string param2;
    private LapTopScript lapTopScript;
    [SerializeField]
    TextMeshProUGUI speakBubble;
    private string formatedString = @"<font=""Foul Fiend SDF""><mark=#000000 padding=""10, 10, 0, 0"">";
    private float targetAlpha;
    private bool notClose = true;
    private Transform player;
    private bool canInteract = true;
    private string interactMessageSave;
    
    private void Start()
    {
        interactMessageSave = base.promptMessage;
        speakBubble = GetComponentInChildren<TextMeshProUGUI>();
        zombiesToKill = Random.Range(25, 70);
        lapTopScript = GetComponentInParent<LapTopScript>();
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        string[] tmp = lapTopScript.GetParams();
        param1 = tmp[0];
        param2 = tmp[1];
    }
    public void AddKilledZombie()
    {
        if (firstInteract) return;
        killedZombies += 1;
    }
    private void Update()
    {
        if(speakBubble.color.a >= 1f)
        {
            notClose = Vector3.Distance(this.transform.position, player.position) >= 5f;
        }
        else
        {
            notClose = true;
        }
        
        if (notClose)
        {
            float alpha = Mathf.MoveTowards(speakBubble.color.a, 0f, .5f * Time.deltaTime);
            speakBubble.color = new Color(speakBubble.color.r, speakBubble.color.g, speakBubble.color.b, alpha);
        }
        if (speakBubble.color.a <= 0f)
        {
            canInteract = true;
            base.promptMessage = interactMessageSave;
        }
    }
    protected override void Interact()
    {
        if (!canInteract) return;
        
        canInteract = false;
        base.promptMessage = "";
        string outputString = "";
        if (gotParams)
        {
            outputString = $"{formatedString}Hi. I already told you, but here are the parameters again: {param1},{param2}</font>";
        }
        else if (firstInteract)
        {
            firstInteract = false;
            outputString = $"{formatedString}Hello there! Would you like to know the parameters of the laptop? Well, here's an intriguing offer for you: If you can eliminate {zombiesToKill} zombies, I'll be more than happy to provide you with the information.</font>";
        }
        else
        {
            if(killedZombies >= zombiesToKill)
            {
                outputString = $"{formatedString}Nice work! You killed {killedZombies} zombies. The paramters are: {param1},{param2}</font>";
            }
            else
            {
                outputString = $"{formatedString}What do you want. You only killed {killedZombies} zombies. Kill {zombiesToKill- killedZombies} more!</font>";
            }            
        }
        speakBubble.color = new Color(speakBubble.color.r, speakBubble.color.g, speakBubble.color.b, 1f);
        speakBubble.text = outputString;
        base.Interact();
    }
}
