using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullHealthItem : MonoBehaviour
{
    [SerializeField]
    private GameObject effectPickedUp;
    private AudioSource pickUpSound;
    [SerializeField]
    private float timetodestroy;
    private GameObject playerObject;
    private bool pickedUp = false;
    // Start is called before the first frame update
    void Start()
    {
        pickUpSound = GetComponent<AudioSource>();
        playerObject = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
        if(!pickedUp && transform.localScale.x <= .3f)
        {
            Destroy(this.gameObject);
        }
        if (pickedUp)
        {
            effectPickedUp.transform.position = new Vector3(playerObject.transform.position.x, effectPickedUp.transform.position.y, playerObject.transform.position.z);
            //playerObject.GetComponent<PlayerHealth>().RestoreHealth(500);
        }
        else
        {
            float scale = Mathf.MoveTowards(transform.localScale.x, 0f, timetodestroy * Time.deltaTime);
            this.transform.localScale = new Vector3(scale, scale, scale);
        }
    }
    private void SetStaminaAndHealthBack()
    {
        playerObject.GetComponent<PlayerMotor>().StaminaUnlimited(0);
        playerObject.GetComponent<PlayerHealth>().SetGodmode(0);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (pickedUp) return;
        if (other.gameObject == playerObject)
        {
            pickedUp = true;
            playerObject.GetComponent<PlayerHealth>().SetGodmode(1);
            playerObject.GetComponent<PlayerMotor>().StaminaUnlimited(1);
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
            {
                effectPickedUp = Instantiate(effectPickedUp, new Vector3(playerObject.transform.position.x, hit.transform.position.y, playerObject.transform.position.z), effectPickedUp.transform.rotation);
                effectPickedUp.transform.localScale = new Vector3(2, 2, 2);
            }
            
            foreach (Transform t in GetComponentsInChildren<Transform>())
            {
                if (t != this.transform)
                {
                    t.gameObject.SetActive(false);
                }
            }
            pickUpSound.Play();
            Invoke("SetStaminaAndHealthBack", 18.5f);
            Destroy(this.gameObject, 20f);
            Destroy(effectPickedUp, 20f);
        }
    }
}
