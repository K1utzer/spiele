using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LapTopScript : Interactable
{
    [SerializeField]
    GameObject LaptopInputUI;
    [SerializeField]
    private TMP_InputField inputTextField;
    [SerializeField]
    private TextMeshProUGUI LaptopText;
    [SerializeField]
    private GameObject ConsoleText;
    private GameObject[] ConsoleTexts;
    [SerializeField]
    private GameObject Door;

    private string username = "TEST1";
    private string password = "TEST2";
    private string param1 = "param1";
    private string param2 = "param2";
    private string laptoptext = "";
    private string laptoptextEnd = ")\n\tOpenDoor(util)";

    private string[] outputText;

    private bool correct = false;
    private bool processing = false;
    private bool consoleOutput = false;

    public AudioSource correct_code;
    public AudioSource wrong_code;

    private int correctValue = 0;
    private string tmp_promtmessage;

    public void UpdateLaptopText()
    {
        LaptopText.text = laptoptext+$"{param1}, {param2}" +laptoptextEnd;
    }
    public string[] GetParams()
    {
        return new string[]{username, password};
    }
    public string GetCode()
    {
        return $"{username},{password}";
    }
    public void OpenDoor()
    {
        inputTextField.text = $"{username},{password}";
        EnterButton();
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().SetUnlockedLevel(2);
        Destroy(Door);          
    }
    IEnumerator UpdateConsoleOutput()
    {
        consoleOutput = true;
        for (int c = 0; c < outputText.Length; c++)
        {
            ConsoleTexts[c].GetComponent<TextMeshProUGUI>().text = outputText[c];
            if (c == 4)
            {
                ConsoleTexts[c].GetComponent<TextMeshProUGUI>().color = Color.green;
            }
            if (c == 13)
            {
                if(correctValue == 3)
                {
                    ConsoleTexts[c].GetComponent<TextMeshProUGUI>().color = Color.green;
                }
                else
                {
                    ConsoleTexts[c].GetComponent<TextMeshProUGUI>().color = Color.red;
                }
            }
            if (c == 13)
            {
                if (correctValue == 1 || correctValue == 2)
                {
                    ConsoleTexts[c].GetComponent<TextMeshProUGUI>().color = Color.yellow;
                }
                if (correctValue == 0)
                {
                    ConsoleTexts[c].GetComponent<TextMeshProUGUI>().color = Color.red;
                }
            }

            yield return new WaitForSeconds(Random.Range(.2f, 2.5f));
        }
        if (correctValue == 3)
        {
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().SetUnlockedLevel(2);
            correct_code.Play();
            Destroy(Door, 1f);
        }
        else if (correctValue == 2)
        {
            wrong_code.Play();
            param2 = "param2";            
        }
        else if (correctValue == 1)
        {
            wrong_code.Play();
            param1 = "param1";
        }
        else if (correctValue == 0)
        {
            wrong_code.Play();
            param1 = "param1";
            param2 = "param2";
        }
        inputTextField.text = $"{param1},{param2}";
        UpdateLaptopText();
        processing = false;
        consoleOutput = false;
        base.promptMessage = tmp_promtmessage;
    }

    // Start is called before the first frame update
    void Start()
    {
        username = ParameterLists.names[Random.Range(0, ParameterLists.names.Length)].ToLower();
        password = ParameterLists.passwords[Random.Range(0, ParameterLists.passwords.Length)].ToLower();

        laptoptext = $"def OpenDoor(util):\n\tDoor door_1og = Door(util)\n\tdoor.open()\n\nif __name__ == '__main__':\n\t#TODO: Missing parameters in Utility\n\t#Search for hints in 1.OG\n\tUtility util = Utility(";
        UpdateLaptopText();

        ConsoleTexts = new GameObject[ConsoleText.transform.childCount];

        outputText = new string[ConsoleText.transform.childCount];
        for (int i = 0; i < ConsoleText.transform.childCount; i++)
        {
            ConsoleTexts[i] = ConsoleText.transform.GetChild(i).gameObject;
            ConsoleTexts[i].GetComponent<TextMeshProUGUI>().text = "";
        }
        ConsoleTexts[0].GetComponent<TextMeshProUGUI>().text = "TWO CREDENTIAL PARAMETERS ARE MISSING...";
        ConsoleTexts[1].GetComponent<TextMeshProUGUI>().text = "MAYBE BOB CAN HELP YOU";

        outputText[0] = "Loading hardware utilities...";
        outputText[1] = "Initializing exploit framework...";
        outputText[2] = "Executing reconnaissance tools...";
        outputText[3] = "Deploying door modules...";
        outputText[4] = "Utilities loaded and ready for operation."; // color green
        outputText[5] = "Initializing network driver...";
        outputText[6] = "Loading kernel modules...";
        outputText[7] = "Configuring network interfaces...";
        outputText[8] = "Driver initialized. Ready for door control";
        outputText[9] = "Loading hardware utilities...";
        outputText[10] = "Establishing secure connection to target server...";
        outputText[11] = "Handshaking with remote host...";
        outputText[12] = "Authenticating credentials...";

        //inputTextField.GetComponent<TMP_InputField>();
    }
    IEnumerator InvalidInputConsoleOutput()
    {
        foreach (GameObject output in ConsoleTexts)
        {
            output.GetComponent<TextMeshProUGUI>().text = "INVALID INPUT!!";
            output.GetComponent<TextMeshProUGUI>().color = Color.red;
            yield return new WaitForSeconds(Random.Range(.2f, .8f));
        }
        processing = false;
    }
    public void EnterButton()
    {
        string[] inputText = inputTextField.text.Split(',');

        try
        {
            param1 = inputText[0].ToLower();
            param2 = inputText[1].ToLower();
            UpdateLaptopText();
        }
        catch
        {            
            StartCoroutine(InvalidInputConsoleOutput());
            LaptopInputUI.SetActive(false);
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().UnPause();
            return;
        }       
        
        if (param1 == username && param2 == password)
        {
            outputText[13] = "Connection established. Access granted."; //color green
            outputText[14] = "Door opening...";
            correct = true;
            correctValue = 3;
            //CONSOLEN AUSGABE CORRECT UND T�RE �FFNEN
        }
        else if(param1 == username)
        {
            outputText[13] = "Connection failed. Access denied"; //color red
            outputText[14] = "Incorrect Parameter: param2"; // color orange
            correctValue = 2;
        }
        else if (param2 == password)
        {
            outputText[13] = "Connection failed. Access denied"; //color red
            outputText[14] = "Incorrect Parameter: param1"; // color orange
            correctValue = 1;
        }
        else
        {
            outputText[13] = "Connection failed. Access denied"; //color red
            outputText[14] = "TRY AGAIN!!!!!11!";
            correctValue = 0;
        }
        StartCoroutine(UpdateConsoleOutput()); 
        LaptopInputUI.SetActive(false);
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().UnPause();
    }
    public void CancelButton()
    {
        LaptopInputUI.SetActive(false);
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().UnPause();
    }

    protected override void Interact()
    {
        if (!correct && !processing)
        {
            foreach (GameObject output in ConsoleTexts)
            {
                output.GetComponent<TextMeshProUGUI>().text = "";
                output.GetComponent<TextMeshProUGUI>().color = Color.white;
            }
            tmp_promtmessage = base.promptMessage;
            base.promptMessage = "";
            processing = true;
            LaptopInputUI.SetActive(true);
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().LaptopEnter();
            base.Interact();
        }        
    }
}
