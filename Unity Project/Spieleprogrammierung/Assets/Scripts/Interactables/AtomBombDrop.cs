using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomBombDrop : MonoBehaviour
{
    [SerializeField]
    private GameObject explosion;
    private AudioSource pickUpSound;
    [SerializeField]
    private float timetodestroy;
    private GameObject playerObject;
    private bool pickedUp = false;
    // Start is called before the first frame update
    void Start()
    {
        pickUpSound = GetComponent<AudioSource>();
        playerObject = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
        if(!pickedUp && transform.localScale.x <= .3f)
        {
            Destroy(this.gameObject);
        }
        if (pickedUp)
        {
            Time.timeScale = Mathf.MoveTowards(Time.timeScale, 1f, .5f * Time.deltaTime);
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().KillAllZombies(explosion);
        }
        else
        {
            float scale = Mathf.MoveTowards(transform.localScale.x, 0f, timetodestroy * Time.deltaTime);
            this.transform.localScale = new Vector3(scale, scale, scale);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (pickedUp) return;
        if (other.gameObject == playerObject)
        {
            pickedUp = true;
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().KillAllZombies(explosion);
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
            {
                explosion = Instantiate(explosion, new Vector3(playerObject.transform.position.x, hit.transform.position.y, playerObject.transform.position.z), explosion.transform.rotation);
                explosion.transform.localScale = new Vector3(2, 2, 2);
            } 
            pickUpSound.Play();
            foreach (Transform t in GetComponentsInChildren<Transform>(true))
            {
                if (t != this.transform)
                {
                    t.gameObject.SetActive(false);
                }
            }
            Time.timeScale = .3f;
            StartCoroutine(End());
        }
    }
    IEnumerator End()
    {
        yield return new WaitForSeconds(4f);
        Time.timeScale = 1f;
        Destroy(this.gameObject);        
    }
}
