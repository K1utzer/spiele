using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicKit : Interactable
{
    private int usedSpawnPoint = -1; // -1 ist zombiedrop
    private int restoreHealthAmount = 20;
    private PlayerHealth player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        restoreHealthAmount = Random.Range(5, 20);
    }
    public void SetSpawnPoint(int spawnPoint)
    {
        usedSpawnPoint = spawnPoint;
    }
    public int GetSpawnPoint()
    {
        return usedSpawnPoint;
    }
    public int GetHealthAmount()
    {
        return restoreHealthAmount;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    protected override void Interact()
    {
        if (player.RestoreHealth(restoreHealthAmount))
        {
            if(usedSpawnPoint != -1) GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().FreeSpawnPoint(usedSpawnPoint);
            Destroy(this.gameObject);            
        }
        base.Interact();
    }
}
