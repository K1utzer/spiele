using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class DroneTurret : Interactable
{
    private bool canBuy = true;
    [SerializeField]
    private int secondsToWait;
    private int currentSecondsToWait;
    [SerializeField]
    private Transform turretBox;
    [SerializeField]
    private Transform[] attackPoints;
    [SerializeField]
    private GameObject muzzleFlash;
    [SerializeField]
    private GameObject gun;
    private string prombtMessageBackup;
    private bool AllowedToShoot = true;
    [SerializeField]
    private int SecondsActive;
    private int currentSecondsActive;
    [SerializeField]
    private Animator propellerAnimator;
    private Animator animator;
    private Quaternion rot;
    private bool activeShooting = true;
    [SerializeField]
    LayerMask layermask;
    [SerializeField]
    Transform detectPoint;
    [SerializeField]
    int damage;
    [SerializeField]
    int cost;
    [SerializeField]
    GameObject bullet;
    private Quaternion startPosTurret;
    private Quaternion startPosGun;
    private bool aimOnZomb = false;
    private float StartTimeSound = 0;
    [SerializeField]
    private AudioSource ShotSound;
    [SerializeField]
    private AudioSource start;
    [SerializeField]
    private AudioSource running;
    [SerializeField]
    private AudioSource end;
    [SerializeField]
    TextMeshProUGUI droneText;
    // Start is called before the first frame update
    void Start()
    {
        try
        {
            cost -= 20 / Difficulty.difficulty;
            SecondsActive += 20 / Difficulty.difficulty;
        }
        catch { }

        droneText.text = $"{cost}$\nfor\n{SecondsActive} sec";
        base.promptMessage = $"Activate for {cost}$ (E)";

        startPosTurret = transform.rotation;
        startPosGun = gun.transform.rotation;
        animator = GetComponent<Animator>();
        prombtMessageBackup = base.promptMessage;
        currentSecondsToWait = secondsToWait;
        currentSecondsActive = SecondsActive;
        Interact();
    }
    /*
     * if (muzzleFlash != null)
        {
            GameObject hiteffect = Instantiate(muzzleFlash, attackPoint.position, attackPoint.rotation).gameObject;
            Destroy(hiteffect, 1f);
        }
        */
    // Update is called once per frame
    void Update()
    { 
        
    }

    private void Shoot(Vector3 zomb_pos)
    {
        float shootforceShot = 500;
        zomb_pos.y -= 1.5f;
        Transform attkPoint = attackPoints[UnityEngine.Random.Range(0, attackPoints.Length)];
        GameObject currentBullet = Instantiate(bullet, attkPoint.position, attkPoint.rotation); //store instantiated bullet in currentBullet
        currentBullet.GetComponent<Bullet>().damage = damage;
        Vector3 directionWithoutSpread;
        directionWithoutSpread = zomb_pos - attkPoint.position;//attkPoint.forward.normalized;
        currentBullet.transform.forward = directionWithoutSpread;
        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithoutSpread.normalized * shootforceShot, ForceMode.Impulse);
        if (ShotSound != null) ShotSound.Play();
        if (muzzleFlash != null)
        {
            GameObject hiteffect = Instantiate(muzzleFlash, attkPoint.position, attkPoint.rotation).gameObject;
            Destroy(hiteffect, 1f);
        }
        /*if (activeShooting)
        {
            Invoke("Shoote", 1f);
        }*/
        
    }
    private void ResetShot()
    {
        activeShooting = true;
    }
    private Transform GetClosestZombie()
    {
        GameObject[] zombies = GameObject.FindGameObjectsWithTag("Zombie");
        GameObject[] naod = GameObject.FindGameObjectsWithTag("ZombieEnd");
        GameObject[] all_zombies = zombies.Concat(naod).ToArray();
        float closestDistance = Mathf.Infinity;
        Transform trans = null;
        foreach(GameObject go in all_zombies)
        {
            bool zomb = go.GetComponent<ZombieController>() != null ? go.GetComponent<ZombieController>().isDead : go.GetComponentInParent<EndBossController>().isDead;
            if (zomb) continue;
            float currentDistance = Vector3.Distance(detectPoint.position, go.transform.position);
            if (go.transform.position.y < 8.8 || currentDistance > 25f) continue;// Auf Dach und in Reichweite?
            if (currentDistance < closestDistance)
            {
                closestDistance = currentDistance;
                trans = go.transform;
            }
                
        }
        return trans;
    }
    IEnumerator Fire()
    {
        yield return new WaitForSeconds(3f);
        animator.SetBool("flying", true);
        StartCoroutine(Deactivate());
        
        while (AllowedToShoot)
        {
            Transform zomb = GetClosestZombie();
            if(zomb != null)
            {
                Vector3 zomb_pos = zomb.position;
                zomb_pos.y += 2.5f;
                rot = Quaternion.LookRotation((zomb_pos - transform.position).normalized);                
                rot *= Quaternion.Euler(Vector3.up * 90);
                //gun.transform.rotation = rot;
                gun.transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 8f);
                rot.x = 0;
                rot.z = 0;
                Ray ray = new Ray(this.transform.position, zomb_pos);
                Debug.DrawRay(ray.origin, ray.direction * 200f, Color.red);
                //transform.rotation = rot;                
                transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 10f);
                //if (transform.rotation.y <= rot.y+.8f && transform.rotation.y >= rot.y-.8f)
                //{
                    if (Time.time >= StartTimeSound + .2f)
                    {
                        Shoot(zomb_pos);
                        StartTimeSound = Time.time;
                    }
                //}
            }
            else
            {
                rot = startPosTurret;
                transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 1f);
                gun.transform.rotation = Quaternion.Slerp(transform.rotation, startPosGun, Time.deltaTime * 1f);
            }
           yield return new WaitForSeconds(.01f);
        }
        StartCoroutine(Activate());
    }
    IEnumerator Deactivate()
    {
        while(currentSecondsActive > 0)
        {
            yield return new WaitForSeconds(1f);
            base.promptMessage = $"Active for {currentSecondsActive} sec";
            currentSecondsActive -= 1;
        }
        currentSecondsActive = SecondsActive;
        animator.SetBool("flying", false);
        AllowedToShoot = false;
        yield return new WaitForSeconds(3f);
        running.Stop();
        end.Play();
        yield return new WaitForSeconds(2f);
        propellerAnimator.SetBool("flying", false);
    }
    IEnumerator Activate()
    {
        while (currentSecondsToWait > 0)
        {
            yield return new WaitForSeconds(1f);
            base.promptMessage = $"Wait {currentSecondsToWait} sec to use again.";
            currentSecondsToWait -= 1;
        }
        base.promptMessage = prombtMessageBackup;
        currentSecondsToWait = secondsToWait;
        canBuy = true;
    }
    protected override void Interact()
    {
        if (!canBuy) return;
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMotor>().RemoveMoney(cost))
        {
            canBuy = false;
            start.Play();
            running.PlayDelayed(start.time);
            base.promptMessage = $"Active for {currentSecondsActive} sec";
            AllowedToShoot = true;
            propellerAnimator.SetBool("flying", true);
            StartCoroutine(Fire());
        }            
        base.Interact();
    }
    private Transform FindComponentsInChildWithTag(Transform parent, string tag)
    {
        Transform[] t = parent.GetComponentsInChildren<Transform>(true);
        List<Transform> transforms = new List<Transform>();

        foreach (Transform tr in t)
        {
            if (tr.tag == tag)
            {
                return tr;
            }
        }
        return null;
    }
}
