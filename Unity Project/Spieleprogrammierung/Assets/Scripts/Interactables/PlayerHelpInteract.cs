using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerHelpInteract : Interactable
{
    private int messageCounter = 0;
    [SerializeField]
    TextMeshProUGUI speakBubble;
    private string formatedString = @"<font=""Foul Fiend SDF""><mark=#000000 padding=""10, 10, 0, 0"">";
    private string formatedStringEnd = @"</font>";
    private float targetAlpha;
    private bool notClose = true;
    private Transform player;
    private bool canInteract = true;
    private string[] Messages;
    private string promtMessageBackup;
    
    private void Start()
    {
        promtMessageBackup = base.promptMessage;
        Messages = new string[] {"Hello, I'll give you helpful tips! Come back later, and each time I'll provide you with different tips.",
            "Zombies have a chance to drop health kits and ammo. Additionally, they can drop power-ups that provide temporary benefits, but these power-ups disappear after a certain period of time. Therefore, it is advisable not to wait too long to grab them.",
            "Health kits and Ammo is spawned randomly on the map, you just have to find it",
            "To access the next floors, open the doors. The codes to open the doors are hidden on the map. However, the codes are not initially available; they are randomly spawned after certain waves."};
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    private void Update()
    {
        if(speakBubble.color.a >= 1f)
        {
            notClose = Vector3.Distance(this.transform.position, player.position) >= 5f;
        }
        else
        {
            notClose = true;
        }
        
        if (notClose)
        {
            float alpha = Mathf.MoveTowards(speakBubble.color.a, 0f, .5f * Time.deltaTime);
            speakBubble.color = new Color(speakBubble.color.r, speakBubble.color.g, speakBubble.color.b, alpha);
        }
        if (speakBubble.color.a <= 0f)
        {
            canInteract = true;
            base.promptMessage = promtMessageBackup;
        }
    }
    protected override void Interact()
    {
        if (!canInteract) return;
        
        canInteract = false;
        base.promptMessage = "";
       
        speakBubble.color = new Color(speakBubble.color.r, speakBubble.color.g, speakBubble.color.b, 1f);
        speakBubble.text = $"{formatedString}{Messages[messageCounter]}{formatedStringEnd}";
        messageCounter += 1;
        if (messageCounter >= Messages.Length) messageCounter = 1;
        base.Interact();
    }
}
