using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollow : MonoBehaviour
{
    [SerializeField]
    GameObject waypointsfirst;
    [SerializeField]
    GameObject waypointssecond;
    [SerializeField]
    GameObject waypointsthird;
    [SerializeField]
    GameObject waypointsfourth;
    private float TimeForSound = 5f;
    private float StartTimeSound;
    int index = 0;
    private List<Waypoints> waypoints;
    private Transform waypoint;
    private float normalY;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Sounds());
        normalY = transform.position.y;
        waypoints = new List<Waypoints>();
        waypoints.Add(new Waypoints(waypointsfirst.GetComponentsInChildren<Transform>()));
        waypoints.Add(new Waypoints(waypointssecond.GetComponentsInChildren<Transform>()));
        waypoints.Add(new Waypoints(waypointsthird.GetComponentsInChildren<Transform>()));
        waypoints.Add(new Waypoints(waypointsfourth.GetComponentsInChildren<Transform>()));
        do
        {
            waypoint = waypoints[index].waypoints[Random.Range(0, waypoints[index].waypoints.Length)];
        } while (!waypoint.gameObject.name.Contains("GameObject"));
    }
    IEnumerator Sounds()
    {
        while (true)            
        {
            TimeForSound = Random.Range(2, 40);
            yield return new WaitForSeconds(TimeForSound);
            AudioSource[] zombieSounds = GetComponentsInChildren<AudioSource>();
            zombieSounds[Random.Range(0, zombieSounds.Length)].Play();

            
        }
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 dest = waypoint.transform.position;
        Vector3 newPos = Vector3.MoveTowards(transform.position, dest, 5*Time.deltaTime);
        transform.position = newPos;
        float dist = Vector3.Distance(transform.position, dest);
        Vector3 direction = waypoint.transform.position - transform.position;
        direction.Set(direction.x, 0, direction.z);
        transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
        transform.position = new Vector3(transform.position.x, normalY, transform.position.z);
        if (dist <= 0.5f)
        {            
            index += 1;
            if(index > 3)
            {
                GetComponent<Animator>().SetTrigger("death"+Random.Range(1,4));
                Destroy(this.gameObject, 5f);
            }
            else
            {
                do
                {
                    waypoint = waypoints[index].waypoints[Random.Range(0, waypoints[index].waypoints.Length)];
                } while (!waypoint.gameObject.name.Contains("GameObject"));                
            }            
        }
    }
}
public class Waypoints
{
    public Transform[] waypoints;

    public Waypoints(Transform[] waypoints)
    {
        this.waypoints = waypoints;
    }
}
