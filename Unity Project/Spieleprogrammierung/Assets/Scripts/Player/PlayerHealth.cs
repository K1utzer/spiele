using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    
    private float health;
    private float lerpTimer;
    private float maxHealth = 100f;
    [Header("Health Bar")]
    public float chipSpeed = 2f;
    public Image frontHealthBar;
    public Image backHealthBar;
    public TextMeshProUGUI text;
    [Header("Damage Overlay")]
    public Image overlay;
    public float duration;
    public float fadeSpeed;
    private float durationTimer;

    private bool godmode;

    public GameObject HitSounds;
    // Start is called before the first frame update
    public void SetGodmode(int number)
    {
        if(number <= 0)
        {
            godmode = false;
        }else if (number >= 1)
        {
            godmode = true;
        }
    }
    void Start()
    {
		maxHealth = Difficulty.health;
        health = maxHealth;
        text.text = $"{health}/{maxHealth}";
        overlay.color = new Color(overlay.color.r, overlay.color.g, overlay.color.b, 0);
    }
    public void IncreaseMaxHealth(int amount)
    {
        maxHealth += amount;
        health = maxHealth;
    }
    // Update is called once per frame
    void Update()
    {
        health = Mathf.Clamp(health, 0, maxHealth);
        UpdateHealthUI();
        text.text = $"{health}/{maxHealth}";
        if(overlay.color.a > 0)
        {
            durationTimer += Time.deltaTime;
            if(durationTimer < duration)
            {
                float tempAlpha = overlay.color.a;
                tempAlpha -= Time.deltaTime * fadeSpeed;
                overlay.color = new Color(overlay.color.r, overlay.color.g, overlay.color.b, tempAlpha);
            }
        }
    }
    public void UpdateHealthUI()
    {
        float fillFront = frontHealthBar.fillAmount;
        float fillBack = backHealthBar.fillAmount;
        float hFraction = health / maxHealth;
        if(fillBack > hFraction)
        {
            frontHealthBar.fillAmount = hFraction;
            backHealthBar.color = Color.red;
            lerpTimer += Time.deltaTime;
            float percentComplete = lerpTimer / chipSpeed;
            percentComplete = percentComplete * percentComplete;
            backHealthBar.fillAmount = Mathf.Lerp(fillBack, hFraction, percentComplete);
        }
        if (fillFront < hFraction)
        {            
            backHealthBar.color = Color.green;
            backHealthBar.fillAmount = hFraction;
            lerpTimer += Time.deltaTime;
            float percentComplete = lerpTimer / chipSpeed;
            percentComplete = percentComplete * percentComplete;
            frontHealthBar.fillAmount = Mathf.Lerp(fillFront, backHealthBar.fillAmount, percentComplete);
        }
    }
    public void TakeDamage(float damage)
    {
        if (godmode) return;
        AudioSource[] hitSounds = HitSounds.GetComponentsInChildren<AudioSource>();
        hitSounds[Random.Range(0, hitSounds.Length)].Play();
        health -= damage;
        lerpTimer = 0f;
        durationTimer = 0f;
        overlay.color = new Color(overlay.color.r, overlay.color.g, overlay.color.b, 1);
        if(health <= 0)
        {
            StartCoroutine(Dead());
        }
    }
    IEnumerator Dead()
    {
        yield return new WaitForSeconds(.5f);
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().GameOver();
    }
    public bool RestoreHealth(float healAmount)
    {
        if(health >= maxHealth)
        {
            return false;
        }
        health += healAmount;
        if (health > maxHealth) health = maxHealth;
        lerpTimer = 0f;
        return true;
    }
    public void DestroyGameObject(GameObject gameObject)
    {
        if (health < maxHealth)
        {
            Destroy(gameObject);
        }       
    }

}
