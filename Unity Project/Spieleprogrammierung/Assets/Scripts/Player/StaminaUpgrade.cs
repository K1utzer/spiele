using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StaminaUpgrade : Interactable
{
    public int cost;
    private GameObject player;
    //public GameObject soundBought;
    public GameObject effectBought; // In effect ist audio on play awake
    public GameObject notEnoughMoneySound;
    [Range(0,10)]
    public int maxLevel;
    private int level = 0;
    public int percentageIncrease;
    private bool isMaxLevel = false;
    private bool canBuy = true;
    private TextMeshProUGUI level_text;
    // Start is called before the first frame update
    void Start()
    {
        level_text = GetComponentInChildren<TextMeshProUGUI>();
        level_text.text = $"{level}/{maxLevel}";
        base.promptMessage = $"Buy for {cost}$ (E)";
        player = GameObject.FindGameObjectWithTag("Player");
    }
    private void MaxLevel()
    {
        isMaxLevel = true;
        base.promptMessage = "MAX LEVEL";
    }
    protected override void Interact()
    {
        if (!canBuy) return;
        canBuy = false;
        if (isMaxLevel) return;
        PlayerMotor pl = player.GetComponent<PlayerMotor>();        
        if (pl.RemoveMoney(cost))
        {
            //soundBought.GetComponent<AudioSource>().Play()
            pl.StaminaUgrade(percentageIncrease);
            level += 1;
            level_text.text = $"{level}/{maxLevel}";
            if (level >= maxLevel)
            {
                isMaxLevel = true;
                base.promptMessage = "MAX LEVEL";
            }

            if (effectBought != null)
            {
                RaycastHit hit;
                
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
                {
                    //hit.transform.CompareTag("Boden");
                    GameObject effect = Instantiate(effectBought, new Vector3(transform.position.x, hit.transform.position.y, transform.position.z), effectBought.transform.rotation);
                    effect.transform.localScale = new Vector3(2, 2, 2);
                }
                /*RaycastHit[] hits = Physics.RaycastAll(transform.position, transform.TransformDirection(Vector3.down), Mathf.Infinity);
                for(int c = 0; c < hits.Length; c++)
                {
                    if (hits[c].transform.CompareTag("Boden"))
                    {
                        GameObject effect = Instantiate(effectBought, new Vector3(player.transform.position.x, hits[c].transform.position.y, player.transform.position.z), effectBought.transform.rotation);
                        effect.transform.localScale = new Vector3(2, 2, 2);
                        break;
                    }
                }*/
                //Destroy(effect, 2f);
            }
        }
        else
        {
            if (notEnoughMoneySound != null)
            {
                GameObject sound = Instantiate(notEnoughMoneySound, transform.position, transform.rotation);
                Destroy(sound, 1f);
            }                     
        }
        base.Interact();
        canBuy = true;
    }
}
