using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMotor : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool isGrounded;
    public float speed = 5f;
    private float fixSpeed;
    public float gravity = -9.8f;
    public float jumpHeight = 1f;

    public AudioSource JumpSound;
    public AudioSource WalkingSound;
    public AudioSource RunningSound;
    public AudioSource FlashLightToggleSound;

    public float sprintIncrease = 3f;
    public Light flashlight;

    private int currentWeapon = 0;
    private GameObject[] weapons = new GameObject[3]; //0: Melee; 1: 2nd weapon; 2: 3rd weapon
    public GameObject melleWeaponPrefab;
    public GameObject defaultWeaponPrefab;

    private bool sprinting = false;
    public bool walking = false;

    public int money = 0;
    private int maxMoney = 9999999;
    public GameObject moneyUI;
    private TextMeshProUGUI moneyUIText;
    public GameObject moneyUIAnimAddPrefab;
    public GameObject moneyUIAnimRemovePrefab;

    [Header("Stamina Bar")]
    public Image frontStaminaBar;
    private float sprintFillTarget;
    private float staminaTime = .3f;
    private float staminaTimeIncr = .3f;
    private float staminaTimeDecr = .3f;

    public float percentageIncrease = 0;
    protected Image[] inv_ui;

    private float elapsedTime;
    public GameObject[] level_weapon1;
    public GameObject[] level_weapon2;

    // Start is called before the first frame update
    void Start()
    {
        sprintFillTarget = 1;
        inv_ui = new Image[2];
        inv_ui[0] = GameObject.FindGameObjectWithTag("Weapon1").GetComponent<Image>();
        inv_ui[1] = GameObject.FindGameObjectWithTag("Weapon2").GetComponent<Image>();

        moneyUIText = moneyUI.GetComponentInChildren<TextMeshProUGUI>();
        moneyUIText.text = $"{money}$";
        Transform arm_transform = GameObject.FindGameObjectWithTag("Arm").GetComponent<Transform>();
        weapons[currentWeapon] = Instantiate(melleWeaponPrefab, arm_transform.position, arm_transform.rotation);
        weapons[currentWeapon].transform.parent = arm_transform;
        weapons[1] = null;
        weapons[2] = null;
        SetWeapon(defaultWeaponPrefab);

        controller = GetComponent<CharacterController>();
        fixSpeed = speed;
    }
    public GameObject GetCurrentWeapon()
    {
        return weapons[currentWeapon];
    }
    public void ChangeWeapon(int to_weapon)
    {
		GameObject tmpModell = GameObject.FindGameObjectWithTag("WeaponModel");
        if (tmpModell != null && tmpModell.GetComponent<WeaponController>().aiming) return;
        if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().inUI)
        {
            return;
        }
        if (weapons[currentWeapon].GetComponentInChildren<WeaponController>().reloading)
        {
            return;
        }
        if (weapons[currentWeapon].GetComponentInChildren<WeaponController>().shooting && !weapons[currentWeapon].GetComponentInChildren<WeaponController>().readyToShoot)
        {
            return;
        }
        if(to_weapon == currentWeapon || weapons[to_weapon] == null)
        {
            return;
        }
        weapons[currentWeapon].SetActive(false);
        currentWeapon = to_weapon;
        weapons[currentWeapon].SetActive(true);
        SetUIWeapon();
    }

    public void SetUIWeapon()
    {
        if (currentWeapon == 0)
        {
            if (weapons[1] != null)
            {
                inv_ui[0].color = new Color(0, 0, 0, 0.5f);
                foreach (var t in level_weapon2)
                {
                    if (t.CompareTag("DMG"))
                    {
                        t.GetComponent<TextMeshProUGUI>().text = $"DMG: {weapons[1].GetComponentInChildren<WeaponController>().damageUpgradeLevel}";
                    }
                    if (t.CompareTag("ROF"))
                    {
                        t.GetComponent<TextMeshProUGUI>().text = $"ROF: {weapons[1].GetComponentInChildren<WeaponController>().fireRateUpgradeLevel}";
                    }
                    t.GetComponent<TextMeshProUGUI>().color = new Color(t.GetComponent<TextMeshProUGUI>().color.r, t.GetComponent<TextMeshProUGUI>().color.g, t.GetComponent<TextMeshProUGUI>().color.b, 0.5f);
                }
            }
            if (weapons[2] != null)
            {
                inv_ui[1].color = new Color(0, 0, 0, 0.5f);
                foreach (var t in level_weapon2)
                {
                    if (t.CompareTag("DMG"))
                    {
                        t.GetComponent<TextMeshProUGUI>().text = $"DMG: {weapons[2].GetComponentInChildren<WeaponController>().damageUpgradeLevel}";
                    }
                    if (t.CompareTag("ROF"))
                    {
                        t.GetComponent<TextMeshProUGUI>().text = $"ROF: {weapons[2].GetComponentInChildren<WeaponController>().fireRateUpgradeLevel}";
                    }
                    t.GetComponent<TextMeshProUGUI>().color = new Color(t.GetComponent<TextMeshProUGUI>().color.r, t.GetComponent<TextMeshProUGUI>().color.g, t.GetComponent<TextMeshProUGUI>().color.b, 0.5f);
                }
            }

        }
        else if (currentWeapon == 1)
        {
            inv_ui[0].sprite = weapons[currentWeapon].GetComponentInChildren<WeaponController>().weapon_img;
            inv_ui[0].color = new Color(0, 0, 0, 1);
            foreach (var t in level_weapon1)
            {
                t.SetActive(true);
                if (t.CompareTag("DMG"))
                {
                    t.GetComponent<TextMeshProUGUI>().text = $"DMG: {weapons[currentWeapon].GetComponentInChildren<WeaponController>().damageUpgradeLevel}";
                }
                if (t.CompareTag("ROF"))
                {
                    t.GetComponent<TextMeshProUGUI>().text = $"ROF: {weapons[currentWeapon].GetComponentInChildren<WeaponController>().fireRateUpgradeLevel}";
                }
                t.GetComponent<TextMeshProUGUI>().color = new Color(t.GetComponent<TextMeshProUGUI>().color.r, t.GetComponent<TextMeshProUGUI>().color.g, t.GetComponent<TextMeshProUGUI>().color.b, 1f);
            }
            if (weapons[2] != null)
            {
                inv_ui[1].sprite = weapons[2].GetComponentInChildren<WeaponController>().weapon_img;
                inv_ui[1].color = new Color(0, 0, 0, 0.5f);                
                foreach (var t in level_weapon2)
                {
                    if (t.CompareTag("DMG"))
                    {
                        t.GetComponent<TextMeshProUGUI>().text = $"DMG: {weapons[2].GetComponentInChildren<WeaponController>().damageUpgradeLevel}";
                    }
                    if (t.CompareTag("ROF"))
                    {
                        t.GetComponent<TextMeshProUGUI>().text = $"ROF: {weapons[2].GetComponentInChildren<WeaponController>().fireRateUpgradeLevel}";
                    }
                    t.GetComponent<TextMeshProUGUI>().color = new Color(t.GetComponent<TextMeshProUGUI>().color.r, t.GetComponent<TextMeshProUGUI>().color.g, t.GetComponent<TextMeshProUGUI>().color.b, 0.5f);
                }
            }            
        }
        else if (currentWeapon == 2)
        {
            inv_ui[1].sprite = weapons[currentWeapon].GetComponentInChildren<WeaponController>().weapon_img;
            inv_ui[1].color = new Color(0, 0, 0, 1f);
            inv_ui[0].sprite = weapons[1].GetComponentInChildren<WeaponController>().weapon_img;
            inv_ui[0].color = new Color(0, 0, 0, 0.5f);
            foreach (var t in level_weapon2)
            {
                t.SetActive(true);
                if (t.CompareTag("DMG"))
                {
                    t.GetComponent<TextMeshProUGUI>().text = $"DMG: {weapons[currentWeapon].GetComponentInChildren<WeaponController>().damageUpgradeLevel}";
                }
                if (t.CompareTag("ROF"))
                {
                    t.GetComponent<TextMeshProUGUI>().text = $"ROF: {weapons[currentWeapon].GetComponentInChildren<WeaponController>().fireRateUpgradeLevel}";
                }                
                t.GetComponent<TextMeshProUGUI>().color = new Color(t.GetComponent<TextMeshProUGUI>().color.r, t.GetComponent<TextMeshProUGUI>().color.g, t.GetComponent<TextMeshProUGUI>().color.b, 1f);
            }
            foreach (var t in level_weapon1)
            {
                if (t.CompareTag("DMG"))
                {
                    t.GetComponent<TextMeshProUGUI>().text = $"DMG: {weapons[1].GetComponentInChildren<WeaponController>().damageUpgradeLevel}";
                }
                if (t.CompareTag("ROF"))
                {
                    t.GetComponent<TextMeshProUGUI>().text = $"ROF: {weapons[1].GetComponentInChildren<WeaponController>().fireRateUpgradeLevel}";
                }
                t.GetComponent<TextMeshProUGUI>().color = new Color(t.GetComponent<TextMeshProUGUI>().color.r, t.GetComponent<TextMeshProUGUI>().color.g, t.GetComponent<TextMeshProUGUI>().color.b, 0.5f);
            }
        }
    }
    public void AddMoney(int money)
    {
        if (money+this.money > 9999999)
        {
            money = 9999999;
        }
        else
        {            
            this.money += money;
            GameObject tmpAdd = Instantiate(moneyUIAnimAddPrefab, moneyUIText.transform);
            tmpAdd.transform.parent = moneyUIText.transform;
            tmpAdd.GetComponent<MoneyAdd>().SetMoney(money);
        }
        moneyUIText.text = $"{this.money}$";
    }
    public bool RemoveMoney(int money)
    {
        if(money > this.money)
        {
            return false;
        }
        this.money -= money;
        moneyUIText.text = $"{this.money}$";
        GameObject tmpAdd = Instantiate(moneyUIAnimRemovePrefab, moneyUIText.transform);
        tmpAdd.transform.parent = moneyUIText.transform;
        tmpAdd.GetComponent<MoneyRemove>().SetMoney(money);        
        return true;
    }
    public void SetWeapon(GameObject weapon)
    {        
        if(weapon.GetComponentInChildren<WeaponController>().weaponType == 0)
        {
            Destroy(weapons[0]);
            weapons[0] = weapon;
            ChangeWeapon(0);
        }
        else
        {
            Transform arm;
            if (weapons[1] == null)
            {
                arm = GameObject.FindGameObjectWithTag("Arm").GetComponent<Transform>();
                weapons[1] = Instantiate(weapon, arm.position, arm.rotation);
                weapons[1].transform.parent = arm;
                ChangeWeapon(1);
            }
            else if (weapons[2] == null)
            {
                arm = GameObject.FindGameObjectWithTag("Arm").GetComponent<Transform>();
                weapons[2] = Instantiate(weapon, arm.position, arm.rotation);
                weapons[2].transform.parent = arm;
                ChangeWeapon(2);
            }
            else if(currentWeapon != 0)
            {
                if(weapons[currentWeapon].name == weapon.name)
                {
                    weapons[currentWeapon].GetComponentInChildren<WeaponController>().magazineSize = weapon.GetComponent<WeaponController>().magazineSize;
                    return;
                }
                arm = GameObject.FindGameObjectWithTag("Arm").GetComponent<Transform>();
                Destroy(weapons[currentWeapon]);
                weapons[currentWeapon] = Instantiate(weapon, arm.position, arm.rotation);
                weapons[currentWeapon].transform.parent = arm;
                SetUIWeapon();
            }                
        }
    }

    // Update is called once per frame
    void Update()
    {
        SetUIWeapon();
        GameObject tmpModell = GameObject.FindGameObjectWithTag("WeaponModel");
        try
        {
            if (tmpModell != null && tmpModell.GetComponent<WeaponController>().aiming)
            {
                Sprint(false);
            }
            
        }
        catch { }
        isGrounded = controller.isGrounded;
    }
    public void StaminaUnlimited(int number)
    {
        if(number <= 0)
        {
            staminaTimeIncr -= 5000;
            staminaTimeDecr += 5000;
        }
        else{
            staminaTimeIncr += 5000;
            staminaTimeDecr -= 5000;
        }
    }
    public void StaminaUgrade(float perc)
    {
        staminaTimeIncr += staminaTimeIncr * perc / 100;
        staminaTimeDecr -= staminaTimeDecr * perc / 100;
    }
    // Bekommt inputs vom Inputmanager.cs und gibt sie dem Character controller
    public void ProcessMove(Vector2 input)
    {
        if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().inUI)
        {
            return;
        }
        if (input == Vector2.zero)
        {
            walking = false;
        }
        else
        {
            walking = true;
        }

        Vector3 moveDirection = Vector3.zero;
        moveDirection.x = input.x;
        moveDirection.z = input.y;
        controller.Move(transform.TransformDirection(moveDirection) * speed * Time.deltaTime);
        playerVelocity.y += gravity * Time.deltaTime;
        if (isGrounded && playerVelocity.y < 0)
        {
            playerVelocity.y = -2f;
        }
        controller.Move(playerVelocity * Time.deltaTime);
        if (DetectJump())
        {
            WalkingSound.Stop();
            RunningSound.Stop();
            return;
        }
        frontStaminaBar.fillAmount = Mathf.MoveTowards(frontStaminaBar.fillAmount, sprintFillTarget, staminaTime * Time.deltaTime);
        if(frontStaminaBar.fillAmount <= 0)
        {
            Sprint(false);
        }
        if (sprinting)
        {            
            if (!RunningSound.isPlaying)
            {
                RunningSound.Play();
                WalkingSound.Stop();
            }
        }
        else if (walking)
        {
            if (!WalkingSound.isPlaying)
            {
                WalkingSound.Play();
                RunningSound.Stop();
            }
        }
        else
        {
            WalkingSound.Stop();
            RunningSound.Stop();
        }
    }        
    
    public void Jump()
    {
        if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().inUI)
        {
            return;
        }
        if (isGrounded)
        {
            if(frontStaminaBar.fillAmount < 0.2f)
            {
                return;
            }
            frontStaminaBar.fillAmount -= 0.2f;
            //Sprint(false);
            WalkingSound.Stop();
            RunningSound.Stop();
            JumpSound.Play();            
            playerVelocity.y = Mathf.Sqrt(jumpHeight * -3.0f * gravity);
        }
    }
    public bool DetectJump()
    {
        if(playerVelocity.y > 0)
        {
            return true;
        }
        return false;
    }
    public void Sprint(bool pressed)
    {
        if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().inUI)
        {
            sprinting = false;
            return;
        }
        if (frontStaminaBar.fillAmount <= 0)
        {
            sprinting = false;
        }        
        sprinting = pressed;
        if (sprinting)
        {
            sprintFillTarget = 0;
            staminaTime = staminaTimeDecr;
            speed += sprintIncrease;
        }
        else
        {
            sprintFillTarget = 1;
            staminaTime = staminaTimeIncr;
            speed = fixSpeed;
        }
    }
    public void Flashlight()
    {
        if (GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().inUI)
        {
            return;
        }
        flashlight.enabled = !flashlight.enabled;
        FlashLightToggleSound.Play();
    }
}
