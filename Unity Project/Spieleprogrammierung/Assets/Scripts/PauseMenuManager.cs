using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour
{

    [SerializeField]
    private Slider volume;
    [SerializeField]
    private TextMeshProUGUI volume_text;
    [SerializeField]
    private Slider sensitivity;
    [SerializeField]
    private TextMeshProUGUI sensitivity_text;

    [SerializeField]
    private GameObject PauseMenu;
    [SerializeField]
    private GameObject SettingsMenu;
    [SerializeField]
    private GameObject ControlsMenu;
    [SerializeField]
    private GameObject YouSureMenu;
    [SerializeField]
    private GameObject PlayerLook;
    private bool quit;
    // Start is called before the first frame update
    void Start()
    {
        try
        {
            volume.onValueChanged.AddListener((v) =>
            {
                AudioListener.volume = v;
                volume_text.text = v.ToString("0.0");
                PlayerPrefs.SetFloat("Volume", v);
            });
            sensitivity.onValueChanged.AddListener((v) =>
            {
                sensitivity_text.text = v.ToString("0");
                PlayerPrefs.SetFloat("mouseSensitiviy", v);
                PlayerLook.GetComponent<PlayerLook>().LoadSens();
            });

            if (PlayerPrefs.HasKey("Volume"))
            {
                volume.value = PlayerPrefs.GetFloat("Volume");
            }
            else
            {
                volume.value = 1.0f;
            }

            if (PlayerPrefs.HasKey("mouseSensitiviy"))
            {
                sensitivity.value = PlayerPrefs.GetFloat("mouseSensitiviy");
            }
            else
            {
                sensitivity.value = 50f;
            }
        }
        catch
        {

        }
        
    }
    public void ControlsMenuButton()
    {
        PauseMenu.SetActive(false);
        SettingsMenu.SetActive(false);
        ControlsMenu.SetActive(true);
        YouSureMenu.SetActive(false);
    }
    public void MainMenuAndQuitButton(int setting) //0 Quit&dead, 1 Mainmenu&Dead, 2 Quit, 3 Mainmenu
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().CheckAndSaveHighScore();
        if (setting == 0 || setting == 1)
        {
            if (setting == 0)
            {                
                Application.Quit();
            }
            Time.timeScale = 1;
            AudioListener.volume = PlayerPrefs.GetFloat("Volume");            
            SceneManager.LoadScene("MainMenu");
            return;
        }
        if(setting == 2) this.quit = true;

        PauseMenu.SetActive(false);
        SettingsMenu.SetActive(false);
        YouSureMenu.SetActive(true);
    }
    public void YouSureYesButton()
    {
        if (quit)
        {
            Application.Quit();
        }
        Time.timeScale = 1;
        AudioListener.volume = PlayerPrefs.GetFloat("Volume");
        SceneManager.LoadScene("MainMenu");

    }
    public void YouSureCancelButton()
    {
        PauseMenu.SetActive(true);
        SettingsMenu.SetActive(false);
        YouSureMenu.SetActive(false);

    }
    public void SettingsButton()
    {
        PauseMenu.SetActive(false);
        SettingsMenu.SetActive(true);
    }
    public void ResumeButton()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().UnPause();
    }
    public void BackToPauseMenuButton()
    {
        PauseMenu.SetActive(true);
        SettingsMenu.SetActive(false);
        ControlsMenu.SetActive(false);
        YouSureMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
