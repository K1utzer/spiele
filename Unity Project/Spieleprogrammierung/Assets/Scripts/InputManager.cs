using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private PlayerInput playerinput;
    public PlayerInput.OnFootActions onFoot;

    private PlayerMotor motor;

    private PlayerLook look;
    private DebugController debugController;
    
    private WeaponMachanicsHelper weapon;

    // Start is called before the first frame update
    void Awake()
    {
        playerinput = new PlayerInput();
        onFoot = playerinput.OnFoot;
        motor = GetComponent<PlayerMotor>();
        look = GetComponent<PlayerLook>();
        debugController = GameObject.FindGameObjectWithTag("GameManager").GetComponent<DebugController>();

        weapon = GameObject.FindGameObjectWithTag("Arm").GetComponent<WeaponMachanicsHelper>();

        if (motor != null)
        {
            onFoot.Jump.performed += ctx => motor.Jump();
            onFoot.Sprint.performed += ctx => motor.Sprint(true);
            onFoot.SprintReleased.performed += ctx => motor.Sprint(false);
            onFoot.Flashlight.performed += ctx => motor.Flashlight();
            onFoot.Weapon0.performed += ctx => motor.ChangeWeapon(0);
            onFoot.Weapon1.performed += ctx => motor.ChangeWeapon(1);
            onFoot.Weapon2.performed += ctx => motor.ChangeWeapon(2);
        }
        if (weapon != null)
        {
            onFoot.Shoot.performed += ctx => weapon.Shoot(false);
            onFoot.ShootReleased.performed += ctx => weapon.Shoot(true);
            onFoot.Reload.performed += ctx => weapon.Reload();
            onFoot.Aim.performed += ctx => weapon.Aim(true);
            //onFoot.AimReleased.performed += ctx => weapon.Aim(false);
        }
        if(debugController != null)
        {
            onFoot.Console.performed += ctx => debugController.OnToggleDebug();
            onFoot.Return.performed += ctx => debugController.OnReturn();
            onFoot.Tab.performed += ctx => debugController.OnTab();
        }


    }
    // Update is called once per frame
    void Update()
    {

        if (motor != null)
        {
            motor.ProcessMove(onFoot.Movement.ReadValue<Vector2>());
        }
        if(onFoot.OpenMenu.triggered)
        {
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().Pause();
        }
        
    }
    void FixedUpdate()
    {
        
    }
    private void LateUpdate()
    {
        if(look != null)
        {
            look.ProcessLook(onFoot.Look.ReadValue<Vector2>());
        }
        
    }
    private void OnEnable()
    {
        onFoot.Enable(); 
    }
    private void OnDisable()
    {
        onFoot.Disable();
    }
}
