using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BuyWeapon : Interactable
{
    public GameObject weapon;
    public int cost;
    private GameObject player;
    private TextMeshProUGUI costUI;
    public bool canBuy = true;
	private float newCost;
	private float damage;
	private float bulletsPerTap;
    private bool isShotgun;
    private float bullets;
    private void Start()
    {
		damage = weapon.GetComponentInChildren<WeaponController>().damage;
		bulletsPerTap = weapon.GetComponentInChildren<WeaponController>().bulletsPerTap;
        isShotgun = weapon.GetComponentInChildren<WeaponController>().isShotgun;
        player = GameObject.FindGameObjectWithTag("Player");
        costUI = GetComponentInChildren<TextMeshProUGUI>();
        bullets = 1;
        if (isShotgun)
        {
            bullets = 6;
        }
        if (cost <= 0)
		{
			ChangeCost(cost);
			return;
		}
        newCost = ((damage * (bulletsPerTap*bullets*2))/100)*10; // 2*bulletsPerTap*damage/100*10; wieviele standard zombies man mit zwei magazinen töten kann und Geld verdienen würde
		//Debug.Log(gameObject.name+": "+newCost);
		ChangeCost((int)newCost);
	}
    public void ChangeCost(int newCost)
    {
        try
        {
            cost = newCost;
            costUI = GetComponentInChildren<TextMeshProUGUI>();
            costUI.text = $"{cost}$";
			if(newCost <= 0)
			{
				costUI.text = $"";
			}
        }
        catch
        {

        }
        
    }
    protected override void Interact()
    {
        if (!canBuy)
        {
            return;
        }        
        PlayerMotor pl = player.GetComponent<PlayerMotor>();
        WeaponController weaponModell = null;
        try
        {
            weaponModell = GameObject.FindGameObjectWithTag("WeaponModel").GetComponent<WeaponController>();
        }
        catch
        {
        }
        if (weaponModell == null || (weaponModell != null && !weaponModell.aiming && !weaponModell.reloading))
        {
            if (pl.RemoveMoney(cost))
            {
                pl.SetWeapon(weapon);
                if (cost <= 0)
                {
                    GameObject.FindGameObjectWithTag("MysteryBox").GetComponent<MysteryBox>().CloseBasic();
                    GameObject.FindGameObjectWithTag("MysteryBox").GetComponent<MysteryBox>().CloseInstant();
                    Destroy(this.gameObject);
                }
            }
        }
        else
        {
            //TODO: sound das nicht genug money oder ui anzeige            
        }
        base.Interact();
        return;
    }
}
