using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRateUpgrade : Interactable
{
    public int cost;
    private GameObject player;
    //public GameObject soundBought;
    public GameObject effectBought; // In effect ist audio on play awake
    public GameObject notEnoughMoneySound;
    [Range(0, 10)]
    public int maxLevel;
    private bool isMaxLevel = false;
    public int fireRatePercIncrease;
    private bool canBuy = true;
    private string promtBegin;
    // Start is called before the first frame update
    void Start()
    {
        base.promptMessage = $"Buy for {cost}$ (E)";
        promtBegin = base.promptMessage;
        player = GameObject.FindGameObjectWithTag("Player");
    }
    protected override void Interact()
    {
        if (!canBuy) return;
        canBuy = false;
        PlayerMotor pl = player.GetComponent<PlayerMotor>();
        WeaponController weaponModell = null;
        try
        {
            weaponModell = GameObject.FindGameObjectWithTag("WeaponModel").GetComponent<WeaponController>();
        }
        catch
        {
            return;
        }
        if ((weaponModell != null) && weaponModell.AddFireRateLevel(maxLevel) >= maxLevel)
        {
            StartCoroutine(ShowMaxLevel());
            canBuy = false;
            return;
        }
        if ((weaponModell != null && !weaponModell.aiming && !weaponModell.reloading) && pl.RemoveMoney(cost))
        {

            //soundBought.GetComponent<AudioSource>().Play()
            weaponModell.timeBetweenShots -= (weaponModell.timeBetweenShots * fireRatePercIncrease / 100); 
            weaponModell.timeBetweenShooting -= (weaponModell.timeBetweenShooting * fireRatePercIncrease / 100);
            canBuy = true;
            if (effectBought != null)
            {
                RaycastHit hit;

                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
                {
                    //hit.transform.CompareTag("Boden");
                    GameObject effect = Instantiate(effectBought, new Vector3(transform.position.x, hit.transform.position.y, transform.position.z), effectBought.transform.rotation);
                    effect.transform.localScale = new Vector3(2, 2, 2);
                }
            }
        }
        else
        {
            if(notEnoughMoneySound == null) return;
            GameObject sound = Instantiate(notEnoughMoneySound, transform.position, transform.rotation);
            Destroy(sound, 1f);          
        }
        base.Interact();
        canBuy = true;
    }
    IEnumerator ShowMaxLevel()
    {
        base.promptMessage = "WEAPON ALREADY ON MAX LEVEL";
        yield return new WaitForSeconds(2);
        base.promptMessage = promtBegin;
        canBuy = true;
    }
}
