using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmController : MonoBehaviour
{
    /*private Animator animator;
    private bool isShooting = false;
    private bool isReloading = false;
    private bool canShoot = true;
    private bool firstShot = true;
    //private bool singleShot = false;
    private WeaponController currentWeapon;
    private float TimeOfLastAttack = 0;
    // Start is called before the first frame update
    private float ReloadTimer = 0f;
    private float TimeOfReload = 1f;

    private AudioSource ClipSound;
    private AudioSource ShotSound;
    private AudioSource ReloadSound;
    void Start()
    {
        animator = GetComponent<Animator>();
        //currentWeapon = GetComponent<Inventory>();
        ClipSound = GameObject.FindGameObjectWithTag("ClipSound").GetComponent<AudioSource>();
        ShotSound = GameObject.FindGameObjectWithTag("ShotSound").GetComponent<AudioSource>();
        ReloadSound = GameObject.FindGameObjectWithTag("ReloadSound").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
        UpdateAmmo();
        if (currentWeapon.weapon.magazineCount <= 0)
        {
            Reload();
        }
        if (!isReloading)
        {
            if(Time.time >= (TimeOfLastAttack + currentWeapon.weapon.fireRate))
            {
                canShoot = true;
            }
            if (isShooting)
            {                
                
                if (canShoot && (firstShot || (currentWeapon.weapon.weaponType == WeaponType.AR)))
                {
                    ShotSound.Play();
                    firstShot = false;
                    canShoot = false;
                    TimeOfLastAttack = Time.time;
                    Debug.Log("SHOOT");
                    animator.SetTrigger("Shoot");
                    //currentWeapon.Shooting();
                    currentWeapon.weapon.magazineCount -= 1;
                }
            }
            else
            {
                animator.ResetTrigger("Shoot");
                firstShot = true;
            }
        }
        else
        {
            if (Time.time >= ReloadTimer + TimeOfReload)
            {
                Debug.Log("FINISHED RELOAD");
                isReloading = false;
                if (currentWeapon.weapon.maxAmmo == -1)
                {
                    currentWeapon.weapon.magazineCount = currentWeapon.weapon.magazineSize;
                    return;
                }
                int neededAmmoTofill = currentWeapon.weapon.magazineSize - currentWeapon.weapon.magazineCount;
                int newAmmo = currentWeapon.weapon.maxAmmo - neededAmmoTofill;
                if (newAmmo <= 0)
                {
                    currentWeapon.weapon.magazineCount = currentWeapon.weapon.maxAmmo;
                    currentWeapon.weapon.maxAmmo = 0;
                }
                else
                {
                    currentWeapon.weapon.magazineCount += neededAmmoTofill;
                    currentWeapon.weapon.maxAmmo -= neededAmmoTofill;
                }
            }
        }
    }
    public void UpdateAmmo()
    {
        //currentWeapon.UpdateAmmo();
    }
    public void Shoot()
    {   
        if(currentWeapon.weapon.magazineCount <= 0 || isReloading)
        {
            // Sound click leere Waffe
            ClipSound.Play();
        }
        else if(!isReloading)
        {
            isShooting = true;
            animator.SetFloat("FireRate", 2f - currentWeapon.weapon.fireRate);
        }
    }
    public void ShotReleased()
    {
        isShooting = false;
    }
    public void Reload()
    {
        //TODO: Logik wegen muni in amgazin und noch vorhanden etc.
        if((currentWeapon.weapon.magazineCount < currentWeapon.weapon.magazineSize) && !isReloading)
        {
            if(currentWeapon.weapon.maxAmmo == 0)
            {
                return;
            }
            ReloadSound.Play();
            ReloadSound.pitch = currentWeapon.weapon.reloadSpeed;
            Debug.Log("RELOAD");
            ReloadTimer = Time.time;
            animator.SetFloat("ReloadSpeed", currentWeapon.weapon.reloadSpeed);
            if(currentWeapon.weapon.reloadSpeed == 5)
            {
                TimeOfReload = 0.7f;
            }else if (currentWeapon.weapon.reloadSpeed == 4)
            {
                TimeOfReload = 1;
            }
            else if (currentWeapon.weapon.reloadSpeed == 3)
            {
                TimeOfReload = 1.4f;
            }
            else if (currentWeapon.weapon.reloadSpeed == 2)
            {
                TimeOfReload = 1.9f;
            }
            else if (currentWeapon.weapon.reloadSpeed == 1)
            {
                TimeOfReload = 2.4f;
            }
            animator.SetTrigger("Reload");
            isReloading = true;
            canShoot = false;
            isShooting = false;
        }
        else
        {
            
        }
        
    }*/
}
