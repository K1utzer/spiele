using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammunition : Interactable
{
    private int usedSpawnPoint = -1;
    [SerializeField] bool lightAmmo;
    private int ammoAmount = 20;
    private PlayerMotor player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMotor>();
        ammoAmount = Random.Range(5, 20);
        if (lightAmmo) {
            ammoAmount = Random.Range(5, 20);
        }
    }
    public void SetSpawnPoint(int spawnPoint)
    {
        usedSpawnPoint = spawnPoint;
    }
    public int GetSpawnPoint()
    {
        return usedSpawnPoint;
    }
    public int GetAmmoAmount()
    {
        return ammoAmount;
    }
   
    protected override void Interact()
    {
        GameObject weapon = player.GetCurrentWeapon();
        WeaponController con = weapon.GetComponentInChildren<WeaponController>();
        if (con.weaponType <= 0) return;
        ammoAmount = Random.Range(con.bulletsPerTap * 2, con.bulletsPerTap*5); // Von waffe wo aufgehoben wird 2-5x magazin
        if (lightAmmo)
        {
            ammoAmount = Random.Range(con.bulletsPerTap/3, con.bulletsPerTap); // Von waffe .3-1x magazin
        }
        con.magazineSize = (con.magazineSize + ammoAmount) < con.maxMagazineSize ? con.magazineSize + ammoAmount : con.magazineSize = con.maxMagazineSize;
        con.magazineSize += ammoAmount;
        if(usedSpawnPoint != -1) GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().FreeSpawnPoint(usedSpawnPoint);
        Destroy(this.gameObject);               
        base.Interact();
    }
}
