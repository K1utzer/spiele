using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Weapon", menuName = "Items/Weapon")]
public class Weapon : Item
{
    public GameObject prefab;
    public float fireRate;
    public int damage;
    public int magazineSize;
    public int magazineCount;
    public int maxAmmo;
    public float accuracy;
    public int range;
    public float reloadSpeed;
    public WeaponType weaponType;
}
public enum WeaponType { Melee, Pistol, AR, Shotgun, Sniper}
