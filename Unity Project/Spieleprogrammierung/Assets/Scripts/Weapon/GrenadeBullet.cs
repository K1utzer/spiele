using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GrenadeBullet : Bullet
{
    public GameObject explosion;
    public LayerMask whatIsEnemies;
    //Damage
    public int explosionDamage;
    public float explosionRange;
    public float explosionForce;
    public float maxLifetime;
    private bool exploded = false;

    private void Start()
    {
        Destroy(this.gameObject, 10f);
    }
    private void Update()
    {
        if (collisions > maxCollisions) Explode();
        //Count down lifetime
        maxLifetime -= Time.deltaTime;
        if (maxLifetime <= 0) Explode();
    }
    private void Explode()
    {
        //Instantiate explosion
        if (explosion != null && !exploded)
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
            exploded = true;
        }

        //Check for enemies 
        Collider[] enemies = Physics.OverlapSphere(transform.position, explosionRange, whatIsEnemies);
        int[] tmp = new int[enemies.Length];
        for (int i = 0; i < enemies.Length; i++)
        {
            tmp[i] = (int)(damage / ((transform.position - enemies[i].transform.position).sqrMagnitude));
            
        }
        for (int i = 0; i < enemies.Length; i++)
        {
            if (enemies[i].CompareTag("Zombie"))
            {
                enemies[i].GetComponent<ZombieController>().TakeDamage(tmp[i], 1);
            }else if (enemies[i].CompareTag("ZombieEnd"))
            {
                enemies[i].GetComponentInParent<EndBossController>().TakeDamage(tmp[i]);
            }
            else
            {
                enemies[i].GetComponent<PlayerHealth>().TakeDamage(tmp[i]);
            }            
        }

        //Add a little delay, just to make sure everything works fine
        Invoke("Delay", 0.05f);
    }
    private void Delay()
    {
        Destroy(gameObject);
    }
    /*private void Hit(ZombieController zombie)
    {
        Debug.Log("Hit Zombie");
        GameObject hitEffect = Instantiate(hitPrefab, transform.position, transform.rotation).gameObject;
        zombie.TakeDamage(damage);
        Destroy(this.gameObject);
    }
    private void MissHit()
    {
        Debug.Log("Miss Hit");
        GameObject hitEffect = Instantiate(missHitPrefab, transform.position, transform.rotation).gameObject;
        GameObject missHit = GameObject.FindGameObjectWithTag("MissHit");
        GameObject hitSound = (Instantiate(missHit, transform.position, transform.rotation)).gameObject;
        hitSound.GetComponentInChildren<AudioSource>().Play();
        Destroy(hitSound, 2f);
        Destroy(this.gameObject);
    }*/
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Bullet")) return;
        //Count up collisions
        collisions++;

        //Explode if bullet hits an enemy directly and explodeOnTouch is activated
        if (collision.collider.CompareTag("Zombie") || explodeOnTouch) Explode();
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRange);
    }
}
