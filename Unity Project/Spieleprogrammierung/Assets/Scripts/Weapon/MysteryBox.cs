using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MysteryBox : Interactable
{
    public Material[] gunMaterials;
    public GameObject[] gunModels;
    public Transform spawnpoint;
    private TextMeshProUGUI costUI;
    public int cost;
    private GameObject player;
    public GameObject[] lights;
    private bool opened = false;
    private Vector3 oldBoxSize;
    private GameObject[] spawnedWeapons;
    public GameObject mysteryBoxSound;// Muss 6s lang sein
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        costUI = GetComponentInChildren<TextMeshProUGUI>();
        costUI.text = $"{cost}$";
        oldBoxSize = GetComponent<BoxCollider>().size;
        spawnedWeapons = new GameObject[gunModels.Length];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    protected override void Interact()
    {
        if (opened)
        {
            return;
        }
        PlayerMotor pl = player.GetComponent<PlayerMotor>();
        if (pl.RemoveMoney(cost))
        {
            base.promptMessage = "";
            opened = true;
            Opened();
        }
        else
        {
            //TODO: sound das nicht genug money oder ui anzeige            
        }
        base.Interact();
    }
    private void Opened()
    {
        GetComponent<Animator>().SetBool("open", true);
        if (mysteryBoxSound != null)
        {
            GameObject audio = Instantiate(mysteryBoxSound, spawnpoint.position, spawnpoint.rotation);
            //audio.GetComponent<AudioSource>().Play();
            Destroy(audio, 6.3f);
        }

        foreach (GameObject light in lights)
        {
            light.SetActive(true);
        }
        GetComponent<BoxCollider>().enabled = false;
        StartCoroutine(RandomWeapon());
        
    }
    IEnumerator RandomWeapon()
    {
        for (int c = 0; c < gunModels.Length; c++)
        {
            spawnedWeapons[c] = Instantiate(gunModels[c], spawnpoint.position, spawnpoint.rotation);
            spawnedWeapons[c].transform.parent = this.transform;
            if (c != gunModels.Length-1)
            {
                if (spawnedWeapons[c].gameObject.name.Contains("rifle_08"))
                {
                    Material[] tmp = new Material[spawnedWeapons[c].GetComponentInChildren<Renderer>().materials.Length];
                    for (int c3 = 0; c3 < tmp.Length; c3++)
                    {
                        tmp[c3] = gunMaterials[1];
                    }
                    spawnedWeapons[c].GetComponentInChildren<Renderer>().materials = tmp;
                }else if (!spawnedWeapons[c].gameObject.name.Contains("rifle_07"))
                {
                    Material[] tmp = new Material[spawnedWeapons[c].GetComponentInChildren<Renderer>().materials.Length];
                    for (int c3 = 0; c3 < tmp.Length; c3++)
                    {
                        tmp[c3] = gunMaterials[0];
                    }
                    spawnedWeapons[c].GetComponentInChildren<Renderer>().materials = tmp;
                }                
            }
            else
            {
                Material[] tmpMat = new Material[] { gunMaterials[1], gunMaterials[2], gunMaterials[3], gunMaterials[4] };
                spawnedWeapons[c].GetComponentInChildren<Renderer>().materials = tmpMat;
            }
            spawnedWeapons[c].GetComponentInChildren<BuyWeapon>().promptMessage = "";
            spawnedWeapons[c].GetComponentInChildren<BuyWeapon>().canBuy = false;
            spawnedWeapons[c].GetComponentInChildren<BuyWeapon>().ChangeCost(0);
            try
            {
                spawnedWeapons[c].GetComponentInChildren<TextMeshProUGUI>().gameObject.SetActive(false);
            }
            catch
            {
            }
            spawnedWeapons[c].SetActive(false);
        }
        int weaponCounter = 0;
        int weaponBefore = 0;
        var rand = new System.Random();
        // Braucht 6sekunden insgesamt (F�r musik)
        while (weaponCounter <= 20)
        {
            int randNum;
            do
            {
                randNum = rand.Next(0, spawnedWeapons.Length);
            } while (randNum == weaponBefore);
            spawnedWeapons[weaponBefore].SetActive(false);
            weaponBefore = randNum;
            spawnedWeapons[randNum].SetActive(true);
            weaponCounter++;
            yield return new WaitForSeconds(.3f);
        }
        
        spawnedWeapons[weaponBefore].GetComponentInChildren<BuyWeapon>().canBuy = true;
        spawnedWeapons[weaponBefore].GetComponentInChildren<BuyWeapon>().promptMessage = "Pick up (E)";
        
           
        GetComponent<Animator>().SetFloat("speed", .01f);
        GetComponent<Animator>().SetBool("open", false);
        Transform chest_cover = GameObject.FindGameObjectWithTag("MysteryChestCover").transform;
        while (opened)
        {
            if(chest_cover.rotation.x >= -0.005f)
            {
                break;
            }
            yield return new WaitForSeconds(.5f);
        }
        if (opened)
        {
            CloseBasic();
        }
    }
    public void CloseBasic()
    {
        for(int c = 0; c < spawnedWeapons.Length; c++)
        {
            Destroy(spawnedWeapons[c], 0.1f);
            spawnedWeapons[c] = null;
        }
        foreach (GameObject light in lights)
        {
            light.SetActive(false);
        }
        opened = false;
        GetComponent<BoxCollider>().enabled = true;
        base.promptMessage = "Open mysterybox (E)";
        CloseInstant();
    }
    public void CloseInstant()
    {
        GetComponent<Animator>().SetFloat("speed", 1f);
        GetComponent<Animator>().SetBool("open", false);
    }

}
