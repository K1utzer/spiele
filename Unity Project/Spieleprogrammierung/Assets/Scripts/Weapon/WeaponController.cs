using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    //bullet 
    public GameObject bullet;

    //bullet forceplayp
    public float shootForce, upwardForce;

    //Gun stats
    public float timeBetweenShooting, spread, reloadTime, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    public bool allowButtonHold;
    public int maxMagazineSize;

    int bulletsLeft, bulletsShot;

    public int damage;
    public int weaponZoom;
    public bool canAim = false;

    //bools
    public bool shooting, readyToShoot, reloading, aiming;

    //Reference
    protected Camera fpsCam;
    protected Transform weaponCam;
    protected Vector3 origPos;
    protected float shakeDuration = .5f;
    protected float shakeAmount = 0.05f;
    protected float decreaseFactor = .05f;

    public Transform attackPoint;

    public Sprite weapon_img;
    //Graphics
    public GameObject muzzleFlash;
    protected TextMeshProUGUI AmmoClip;
    protected TextMeshProUGUI AmmoFull;

    public AudioSource ClipSound;
    public AudioSource ShotSound;
    public AudioSource ReloadSound;

    //bug fixing
    public bool allowInvoke = true;
    public bool isShotgun;
    public int ShotgunBullets = 1;
    public int weaponType;

    private bool readyToAim = true;

    private Animator arm;
    private GameObject crosshair;
    private float target_zoom;
    public int damageUpgradeLevel = 0;
    public int fireRateUpgradeLevel = 0;
    //public Weapon weapon;

    private void Start()
    {
        maxMagazineSize = magazineSize;
        arm = GetComponentInParent<Animator>();
        crosshair = GameObject.FindGameObjectWithTag("Crosshair");
        aiming = false;
        ShotgunBullets = 1;
        if (isShotgun)
        {
            ShotgunBullets = 6;
            allowButtonHold = false;
        }
        fpsCam = Camera.main;
        weaponCam = GameObject.FindGameObjectWithTag("WeaponCam").GetComponent<Transform>();
        weaponCam = fpsCam.transform;
        origPos = weaponCam.transform.localPosition;
        target_zoom = fpsCam.fieldOfView;
        AmmoClip = GameObject.FindGameObjectWithTag("AmmoClip").GetComponent<TextMeshProUGUI>();
        AmmoFull = GameObject.FindGameObjectWithTag("AmmoFull").GetComponent<TextMeshProUGUI>();
    }
    private void Awake()
    {
        //make sure magazine is full
        bulletsLeft = bulletsPerTap;
        readyToShoot = true;
        
    }
    void OnEnable()
    {
        UpdateReloadTime();
    }
    public int AddDamageLevel(int maxLevel)
    {
        if (damageUpgradeLevel >= maxLevel) return damageUpgradeLevel;
        damageUpgradeLevel += 1;
        return damageUpgradeLevel;
    }
    public int AddFireRateLevel(int maxLevel)
    {
        if(fireRateUpgradeLevel >= maxLevel) return fireRateUpgradeLevel;
        fireRateUpgradeLevel += 1;
        return fireRateUpgradeLevel; 
    }
    public void UpdateReloadTime()
    {
        float percReload = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMotor>().percentageIncrease;
        reloadTime -= reloadTime * percReload / 100;
    }
    private void Update()
    {
        
        //Set ammo display, if it exists
        if (AmmoClip != null && AmmoFull != null)
        {
            AmmoClip.SetText(""+bulletsLeft);
            AmmoFull.SetText(""+magazineSize);
        }
        fpsCam.fieldOfView = Mathf.MoveTowards(fpsCam.fieldOfView, target_zoom, 100f * Time.deltaTime);
    }
    public virtual void MyInputShoot(bool released)
    {
        if (!reloading && bulletsLeft <= 0 && !aiming)
        {
            Reload();
            return;
        }        
        //Check if allowed to hold down button and take corresponding input
        if (reloading)
        {
            if(ClipSound != null) ClipSound.Play();
            return;
        }
        if (allowButtonHold)
        {            
            shooting = !released;
        }
        else if (released)
        {
            shooting = true;
        }

        //Shooting
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            bulletsShot = 0;
            /*if (isShotgun)
            {
                bulletsLeft--;
                bulletsShot++;
                for (int c = 0; c < 10; c++)
                {
                    Shoot();
                }

            }
            else
            {*/
                Shoot();
            return;
            //}
        }
        if (bulletsLeft <= 0)
        {
            if (ClipSound != null) ClipSound.Play();
        }

    }
    public void AimInput(bool pressed)
    {
        if (canAim && readyToAim && !reloading)
        {
            readyToAim = false;
            aiming = !aiming;
            if (aiming)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLook>().xSensitivity -= GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLook>().xSensitivity / weaponZoom * 10;
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLook>().ySensitivity -= GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLook>().ySensitivity / weaponZoom * 10;
                crosshair.SetActive(false);
                arm.SetBool("zoom", true);
                arm.SetBool("unzoom", true);
                target_zoom = weaponZoom;
                StartCoroutine(FovChange());                
            }
            else
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLook>().LoadSens();
                arm.SetBool("zoom", false);
                target_zoom = 60;
                StartCoroutine(UnZoomDelay());  
				//readyToAim = true;				
            }
        }       
        
    }
    IEnumerator FovChange()
    {
        yield return new WaitForSeconds(.2f);        
        Invoke("ResetZoom", .5f);
    }
    IEnumerator UnZoomDelay()
    {
        yield return new WaitForSeconds(0.2f);
        arm.SetBool("unzoom", false);
        crosshair.SetActive(true);
        Invoke("ResetZoom", .5f);
    }
    private void ResetZoom()
    {
        readyToAim = true;
    }
    public void ReloadInput()
    {
        //Reloading 
        if (bulletsLeft < bulletsPerTap && !reloading && magazineSize > 0 && !aiming) Reload();  
    }

    private void Shoot()
    {
        if (!shooting && bulletsLeft > 0 && !isShotgun)
        {
            return;            
        }
        readyToShoot = false;

        //Find the exact hit position using a raycast
        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); //Just a ray through the middle of your current view

        //check if ray hits something
        Vector3 targetPoint;

        targetPoint = ray.GetPoint(30f); ; //Just a point 30 units vorm punkt

        //Calculate direction from attackPoint to targetPoint
        Vector3 directionWithoutSpread = targetPoint - attackPoint.position;

        int bullets_count = 1;
        if (isShotgun)
        {
            bullets_count = 6;
        }
        for(int c = 0; c < bullets_count; c++)
        {
            //Calculate spread
            float x = Random.Range(-spread, spread);
            float y = Random.Range(-spread, spread);
            float z = Random.Range(-spread, spread);
            float shootforceShot = shootForce;
            if (isShotgun)
            {
                shootforceShot = Random.Range(shootForce - (shootForce * 10/100), shootForce + (shootForce * 10 / 100));
            }
            Vector3 directionWithSpread = directionWithoutSpread + new Vector3(x, y, z);
            //Calculate new direction with spread
            if (aiming && !isShotgun)
            {
                directionWithSpread = directionWithoutSpread;
            }
            if(ShotSound != null) ShotSound.Play();
            //Instantiate bullet/projectile
            
            GameObject currentBullet = Instantiate(bullet, attackPoint.position, attackPoint.rotation); //store instantiated bullet in currentBullet
            currentBullet.GetComponent<Bullet>().damage = damage;
            //Rotate bullet to shoot direction
            currentBullet.transform.forward = directionWithSpread.normalized;

            //Add forces to bullet
            currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootforceShot, ForceMode.Impulse);
            currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.up * upwardForce, ForceMode.Impulse);
        }
        //Instantiate muzzle flash, if you have one
        if (muzzleFlash != null)
        {
            GameObject hiteffect = Instantiate(muzzleFlash, attackPoint.position, attackPoint.rotation).gameObject;
            Destroy(hiteffect, 1f);
        }
            
        
        bulletsLeft--;
        bulletsShot++;
        

        //Invoke resetShot function (if not already invoked), with your timeBetweenShooting
        if (allowInvoke)
        {
            if (!allowButtonHold)
            {
                shooting = false;
            }
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;            
        }
        //if more than one bulletsPerTap make sure to repeat shoot function
        if (bulletsShot < bulletsPerTap && bulletsLeft > 0 && shooting)
           Invoke("Shoot", timeBetweenShots);
    }
    private void ResetShot()
    {
        //Allow shooting and invoking again
        readyToShoot = true;
        allowInvoke = true;
    }

    private void Reload()
    {
		if(magazineSize <= 0) return;
        if (bulletsLeft < bulletsPerTap)
        {
            if (ReloadSound != null)
            {
                ReloadSound.Play();

                if (reloadTime == 1)
                {
                    ReloadSound.pitch = 3;
                }
                else if (reloadTime > 1 && reloadTime <= 2)
                {
                    ReloadSound.pitch = 2;
                }
                else if (reloadTime > 2 && reloadTime <= 3)
                {
                    ReloadSound.pitch = .8f;
                }
                else if (reloadTime > 3 && reloadTime <= 4)
                {
                    ReloadSound.pitch = .6f;
                }
                else if (reloadTime > 4 && reloadTime <= 5)
                {
                    ReloadSound.pitch = .5f;
                }
                else if (reloadTime > 5 && reloadTime <= 6)
                {
                    ReloadSound.pitch = .4f;
                }
            }
            arm.SetBool("reload", true);
            shooting = false;
            readyToShoot = false;
            reloading = true;
			readyToAim = false;
			
            Invoke("ReloadFinished", reloadTime); //Invoke ReloadFinished function with your reloadTime as delay}
        }
    }
    private void ReloadFinished()
    {
        arm.SetBool("reload", false);
        //Fill magazine
        if (magazineSize > bulletsShot)
        {
            int shot = bulletsPerTap - bulletsLeft;
            bulletsLeft += shot;
            magazineSize -= shot;
			if(magazineSize < 0)
			{
				bulletsLeft += magazineSize;
				magazineSize = 0;
			}
            reloading = false;
            readyToShoot = true;
			readyToAim = true;
        }
    }

    /*public Weapon weapon;
    private TextMeshProUGUI AmmoClip;
    private TextMeshProUGUI AmmoFull;
    private Camera cam;
    public Transform missHitPrefab;
    public Transform MuzzleFlashPrefab;
    private Transform MuzzleFlashSpawn;

    private LineRenderer shootingTrail;
    private float shootingTrailDuration = 0.05f;
    private AudioSource missHit;
    
    void Start()
    {
        cam = Camera.main;
        shootingTrail = GetComponent<LineRenderer>();


    }
    public void UpdateAmmo()
    {
        AmmoClip = GameObject.FindGameObjectWithTag("AmmoClip").GetComponent<TextMeshProUGUI>();
        AmmoFull = GameObject.FindGameObjectWithTag("AmmoFull").GetComponent<TextMeshProUGUI>();
        MuzzleFlashSpawn = GameObject.FindGameObjectWithTag("FlashlightSpawn").GetComponent<Transform>();
        if (weapon.maxAmmo <= -1)
        {
            AmmoFull.text = $"\u221E";
        }
        else
        {
            AmmoFull.text = $"{weapon.maxAmmo}";
        }
        AmmoClip.text = $"{weapon.magazineCount}";
    }
    public void MuzzleFlashShot()
    {
        GameObject hitEffect = ((Transform)Instantiate(MuzzleFlashPrefab, MuzzleFlashSpawn.position, MuzzleFlashSpawn.rotation)).gameObject;
        Destroy(hitEffect, 1f);
    }
    public void Shooting()
    {
        MuzzleFlashShot();
        Ray ray = cam.ScreenPointToRay(new Vector3(Screen.width/2, Screen.height / 2));
        RaycastHit hit;
        shootingTrail.SetPosition(0, MuzzleFlashSpawn.position + MuzzleFlashSpawn.forward);
        RaycastHit[] hits;
        hits = Physics.RaycastAll(ray, weapon.range);
        foreach(RaycastHit hitArray in hits)
        {
            if (hitArray.collider.GetComponent<ZombieController>() != null)
            {
                hitArray.collider.GetComponent<ZombieController>().TakeDamage(weapon.damage);
                GameObject hitEffect = ((Transform)Instantiate(hitArray.collider.GetComponent<ZombieController>().bloodParticles, hitArray.point, hitArray.collider.GetComponent<Transform>().rotation)).gameObject;
                shootingTrail.SetPosition(1, hitArray.point);
                StartCoroutine(ShootLine());
                //Destroy(hitEffect, 2f);
                return;
            }
        }
        if (Physics.Raycast(ray, out hit,weapon.range))
        {
            if (hit.collider.GetComponent<ZombieController>() != null)
            {
                hit.collider.GetComponent<ZombieController>().TakeDamage(weapon.damage);
                GameObject hitEffect = ((Transform)Instantiate(hit.collider.GetComponent<ZombieController>().bloodParticles, hit.point, hit.collider.GetComponent<Transform>().rotation)).gameObject;
                shootingTrail.SetPosition(1, hit.point);
                //Destroy(hitEffect, 2f);
            }
            else if (hit.collider.GetComponent<Renderer>() != null)
            {
                Renderer[] childs = missHitPrefab.GetComponentsInChildren<Renderer>();
                foreach(Renderer child in childs)
                {
                    child.sharedMaterial = hit.collider.GetComponent<Renderer>().material;
                    child.sharedMaterial.color = hit.collider.GetComponent<Renderer>().material.color;
                }
                GameObject missHit = GameObject.FindGameObjectWithTag("MissHit");
                GameObject missHitSpawned = Instantiate(missHit, hit.point, hit.collider.GetComponent<Transform>().rotation).gameObject;
                missHitSpawned.GetComponentInChildren<AudioSource>().Play();
                Destroy(missHitSpawned, 2f);
                GameObject hitEffect = ((Transform)Instantiate(missHitPrefab, hit.point, hit.collider.GetComponent<Transform>().rotation)).gameObject;
                shootingTrail.SetPosition(1, hit.point);
            }
            else
            {
                shootingTrail.SetPosition(1, MuzzleFlashSpawn.position + MuzzleFlashSpawn.forward * weapon.range);
            }
            StartCoroutine(ShootLine());
        }
        else
        {
            shootingTrail.SetPosition(1, MuzzleFlashSpawn.position + MuzzleFlashSpawn.forward * weapon.range);
        }


    }
    IEnumerator ShootLine()
    {
        shootingTrail.enabled = true;
        yield return new WaitForSeconds(0.04f);
        shootingTrail.enabled = false;
    }
    protected override void Interact()
    {
        Debug.Log("WEAPON PICKED UP");
        base.Interact();
        Transform arm = GameObject.FindGameObjectWithTag("WeaponSpawnHand").GetComponent<Transform>();
        GameObject test = Instantiate(this.weapon.prefab);
        test.transform.parent = arm.transform;
        test.transform.position = arm.position;
        test.transform.rotation = arm.rotation;
        Inventory inv = GameObject.FindGameObjectWithTag("Arm").GetComponent<Inventory>();
        inv.equipped = this.weapon.weaponType;
        Destroy(this);
    }*/
}
