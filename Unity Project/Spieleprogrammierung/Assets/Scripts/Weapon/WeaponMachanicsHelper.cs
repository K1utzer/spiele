using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMachanicsHelper : MonoBehaviour
{
    private GameManager gameManager;
    private WeaponController weapon;
    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }
    public void Shoot(bool released)
    {
        
        if(weapon != null && !gameManager.inUI)
        {
            weapon.MyInputShoot(released);
        }
    }
    public void Reload()
    {
        if (weapon != null && !gameManager.inUI)
        {
            weapon.ReloadInput();
        }
    }
    public void Aim(bool pressed)
    {
        if (weapon != null && !gameManager.inUI)
        {
            weapon.AimInput(pressed);
        }
    }
    void Update()
    {
        
        weapon = GetComponentInChildren<WeaponController>();
        if (weapon == null)
        {
            weapon = GetComponentInChildren<MeleeController>();
        }
            
    }
}
