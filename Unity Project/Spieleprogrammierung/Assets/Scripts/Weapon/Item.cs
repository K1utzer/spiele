using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject
{
    public string itemName;
    public string description;
    public Sprite icon;


    /*protected override void Interact()
    {        
        base.Interact();
    }*/
    public virtual void Use()
    {
    }
}

