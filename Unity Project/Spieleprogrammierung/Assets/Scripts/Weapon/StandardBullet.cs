using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardBullet : Bullet
{
    
    private void Start()
    {
        Destroy(this.gameObject, 10f);
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 5f))
        {
            if (hit.collider.GetComponent<ZombieController>() != null)
            {
                hit.collider.GetComponent<ZombieController>().TakeDamage(damage, 0);
                GameObject hitEffect = ((Transform)Instantiate(hitPrefab, hit.point, hit.collider.GetComponent<Transform>().rotation)).gameObject;
                Destroy(this.gameObject);
            }
            if(hit.collider.GetComponentInParent<EndBossController>() != null)
            {
                hit.collider.GetComponentInParent<EndBossController>().TakeDamage(damage);
                GameObject hitEffect = ((Transform)Instantiate(hitPrefab, hit.point, hit.collider.GetComponent<Transform>().rotation)).gameObject;
                Destroy(this.gameObject);
            }
        }
    }
    private void Update()
    {
       /* RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, 2f, layermask))
        {
            Physics.Raycast(transform.position, transform.forward, out hit);
            if (hit.collider.GetComponent<ZombieController>() != null)
            {
                Debug.Log("Hit Zombie");
                GameObject hitEffect = Instantiate(hitPrefab, hit.point, hit.collider.GetComponent<Transform>().rotation).gameObject;
                hit.collider.GetComponent<ZombieController>().TakeDamage(damage);
                Destroy(this.gameObject);
            }
            else
            {
                Debug.Log("Hit: "+hit.collider.gameObject.name);
                GameObject hitEffect = Instantiate(missHitPrefab, hit.point, hit.collider.GetComponent<Transform>().rotation).gameObject;
                GameObject missHit = GameObject.FindGameObjectWithTag("MissHit");
                GameObject hitSound = (Instantiate(missHit, hit.point, hit.collider.GetComponent<Transform>().rotation)).gameObject;
                hitSound.GetComponentInChildren<AudioSource>().Play();
                Destroy(hitSound, 2f);
                Destroy(this.gameObject);
            }
        }*/

    }
    private void Hit(ZombieController zombie)
    {
        GameObject hitEffect = Instantiate(hitPrefab, transform.position, transform.rotation).gameObject;
        zombie.TakeDamage(damage, 0);
        Destroy(this.gameObject);
    }
    private void HitBoss(EndBossController zombie)
    {
        GameObject hitEffect = Instantiate(hitPrefab, transform.position, transform.rotation).gameObject;
        
        zombie.TakeDamage(damage);
        Destroy(this.gameObject);
    }
    private void MissHit()
    {
        GameObject hitEffect = Instantiate(missHitPrefab, transform.position, transform.rotation).gameObject;
        GameObject missHit = GameObject.FindGameObjectWithTag("MissHit");
        GameObject hitSound = (Instantiate(missHit, transform.position, transform.rotation)).gameObject;
        hitSound.GetComponentInChildren<AudioSource>().Play();
        Destroy(hitSound, 2f);
        Destroy(this.gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Bullet")) return;
        //GetComponent<Rigidbody>().isKinematic = true;
        if (collision.collider.CompareTag("Zombie"))
        {
            Hit(collision.gameObject.GetComponent<ZombieController>());
            return;
        }
        if (collision.collider.CompareTag("ZombieEnd"))
        {
            HitBoss(collision.gameObject.GetComponentInParent<EndBossController>());
            return;
        }
        MissHit();
    }
}
