using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponStats : Interactable
{
    public float FireRate;
    public int Damage;
    public int MagazineSize;
    public int MaxAmmo;
    public float Accuracy;
    public float ReloadSpeed;

    protected override void Interact()
    {
        
        base.Interact();
    }

}
