using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Transform missHitPrefab;
    public Transform hitPrefab;
    public LayerMask layermask;
    [Range(0f, 1f)]
    public float bounciness;
    public int maxCollisions;
    public bool explodeOnTouch = true;
    PhysicMaterial pysics_mat;
    public int damage;
    protected int collisions;
    void Start()
    {
        pysics_mat = new PhysicMaterial();
        pysics_mat.bounciness = bounciness;
        pysics_mat.frictionCombine = PhysicMaterialCombine.Minimum;
        pysics_mat.bounceCombine = PhysicMaterialCombine.Minimum;
        SphereCollider[] colliders = GetComponents<SphereCollider>();
        foreach (SphereCollider coll in colliders)
        {
            coll.material = pysics_mat;
        }
        Destroy(this.gameObject, 10f);
    }
}
