using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MeleeController : WeaponController
{
    public float range;
    public Transform hitPrefab;
    
    // Start is called before the first frame update
    void Start()
    {
        fpsCam = Camera.main;
        readyToShoot = true;
        AmmoClip = GameObject.FindGameObjectWithTag("AmmoClip").GetComponent<TextMeshProUGUI>();
        AmmoFull = GameObject.FindGameObjectWithTag("AmmoFull").GetComponent<TextMeshProUGUI>();
    }
    public override void MyInputShoot(bool released)
    {
        if (!released && readyToShoot)
        {
            shooting = true;
        }
        if (readyToShoot && shooting)
        {
            
            readyToShoot = false;

            Shoot();
        }
    }
    private void Update()
    {
        AmmoClip.text = "";
        AmmoFull.text = "";
    }
    private void Shoot()
    {
        GetComponentInParent<Animator>().SetTrigger("Shoot");
        Ray ray = fpsCam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, range))
        {
            if (hit.collider.GetComponent<ZombieController>() != null)
            {
                hit.collider.GetComponent<ZombieController>().TakeDamage(damage, -1);
                GameObject hitEffect = ((Transform)Instantiate(hitPrefab, hit.point, hit.collider.GetComponent<Transform>().rotation)).gameObject;
            }
        }
        if (allowInvoke)
        {
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;

        }
        shooting = false;
    }
}
