using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TvHeliCrashTrigger : MonoBehaviour
{
    private Animator heliAnimator;
    private GameObject player;
    private Camera cam;
    private void Start()
    {
        heliAnimator = GameObject.FindGameObjectWithTag("TvHeli").GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").gameObject;
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            heliAnimator.SetBool("crasht", true);
        }        
    }

    private void OnTriggerExit(Collider other)
    {
        heliAnimator.SetBool("crasht", false);
    }
}
